<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5GxBxeEPLJxTByi82fHWPikFzwqDS1/seDzy07n/YxhSDBM1/3pgLwz24W/xo42VrD+MtSu9yM5OoCcq13iEMg==');
define('SECURE_AUTH_KEY',  'qOst+9+Efxtr+ZwQcRoR7JXWCguLeu39Xz+g9qTG39PjG9QzbLhr7ImCBTx3cq8cI4GtZ5QTE15JTK/PwA7eRA==');
define('LOGGED_IN_KEY',    'NoSP1W6w1crI9WEB61ZtYAmgnS4GUPXZR9z4yeoTCvZjb0LhhP8PahHF63pIDNb+WOYHsdzh6VH9Y1PTgmRzTg==');
define('NONCE_KEY',        'WdCWZG66t/eyV8TkqenxBtZtB5T0jL2OS8hQis8HABDhD4jiZ92iwLbZi1IA8xRj50hXsARfvLUT7DHa1K+1Dw==');
define('AUTH_SALT',        'pP3o1stuyc9dBa0X2XfXzFD6jUFQsQbUA0/hd8GhE2ovP4rGvu/mN3GiHdvjLL3b2aTauy009qtRbKwIrKvHIg==');
define('SECURE_AUTH_SALT', 'pn2hSaF90ZxhoeMjDNPu2bDJo4zGGBwVMlVWAtdwyOYDExPBRpMPTdF78PUnirm5VNG5Bqj/4Z+lioEFhzC56Q==');
define('LOGGED_IN_SALT',   'ZgQOsxPqmk+v6b/mfT9mxJPB5yAdWBUs0pb76J4l/PuEUyHCT26EwcluH/DbwRdAHhyOqUKZS41GiXy+DCl/jg==');
define('NONCE_SALT',       'cy4QqhIDoqvcWHLv5x8NlxpdvyAtBEa1lTTyzuXxDMhz6Wc2SgYT9WOPHE4Ubz2s7wCPMc0N1aShIQgshoaFEQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_plqosaq9jx_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define( 'WP_DEBUG', false );
