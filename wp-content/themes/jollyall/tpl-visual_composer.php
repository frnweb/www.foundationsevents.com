<?php /* Template Name: VC Page */
get_header() ;

$meta = _WSH()->get_meta();?>

<?php if( !sh_set( $meta, 'is_home' ) ) get_template_part( 'includes/modules/header/header', 'single' ); ?>

<?php while( have_posts() ): the_post(); ?>
     <?php the_content(); ?>
<?php endwhile;  ?>

<?php get_footer() ; ?>