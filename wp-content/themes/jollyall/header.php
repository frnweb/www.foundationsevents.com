<?php $options = _WSH()->option(); 
/** For maintainer mode */
_WSH()->maintainance();?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
	<title><?php wp_title(''); ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Favicons - Touch Icons -->
	<?php echo ( sh_set( $options, 'site_favicon' ) ) ? '<link rel="icon" type="image/png" href="'.sh_set( $options, 'site_favicon' ).'">': '';?>    
	
	<!-- Support for HTML5 -->
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8WWP3T');</script>
<!-- End Google Tag Manager -->
	
	
	<?php wp_head(); ?>
	<script src="https://embed.small.chat/T02SV1ZP4G6EFWE4NM.js" async></script>
	<!-- Ben Wright's old typkit <link rel="stylesheet" href="https://use.typekit.net/xgf2ptr.css">-->
	<link rel="stylesheet" href="https://use.typekit.net/ogp0eqz.css">
	

</head>


<body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>  
	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8WWP3T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
	<?php 
	/*
	 * hooked _sh_preload()      at priority 5
	 * hooked _sh_sidebar_menu() at priority 10
	 * hooked _sh_nav_and_logo() at priority 15
	*/
	
	do_action( '_sh_after_body_start', $options ); ?>
	
	<div id="main_content">
	
	
	