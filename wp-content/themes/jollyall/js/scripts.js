// JavaScript Document

(function($){
	"use strict";
	var wow_themes = {			
			count: 0,
			tweets: function( options, selector ){
				
				options.action = '_sh_ajax_callback';
				options.subaction = 'tweets';

				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					//dataType:"json",
					success: function(res){
						
						$(selector).html( res );	
					}
				});
				
			},
			
			wishlist: function(options, selector)
			{
				options.action = '_sh_ajax_callback';
				
				if( $(selector).data('_sh_add_to_wishlist') === true ){
					wow_themes.msg( 'You have already done this job', 'error' );
					return;
				}
				
				$(selector).data('_sh_add_to_wishlist', true );

				wow_themes.loading(true);
				
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					dataType:"json",
					success: function(res){

						try{
							var newjason = res;

							if( newjason.code === 'fail'){
								$(selector).data('_sh_add_to_wishlist', false );
								wow_themes.loading(false);
								wow_themes.msg( newjason.msg, 'error' );
							}else if( newjason.code === 'exists'){
								$(selector).data('_sh_add_to_wishlist', true );
								wow_themes.loading(false);
								wow_themes.msg( newjason.msg, 'error' );
							}else if( newjason.code === 'success' ){
								wow_themes.loading(false);
								$(selector).data('_sh_add_to_wishlist', true );
								wow_themes.msg( newjason.msg, 'success' );
							}else if( newjason.code === 'del' ){
								wow_themes.loading(false);
								$(selector).data('_sh_add_to_wishlist', true );
								$(selector).parents('tbody').remove();
								wow_themes.msg( newjason.msg, 'success' );
							}
							
							
						}
						catch(e){
							wow_themes.loading(false);
							$(selector).data('_sh_add_to_wishlist', false );
							wow_themes.msg( 'There was an error while adding product to whishlist '+e.message, 'error' );
							
						}
					}
				});
			},
			likeit: function(options, selector)
			{
				options.action = '_sh_ajax_callback';
				
				if( $(selector).data('_sh_like_it') === true ){
					wow_themes.msg( 'You have already done this job', 'error' );
					return;
				}
				
				$(selector).data('_sh_like_it', true );

				wow_themes.loading(true);
				
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data:options,
					dataType:"json",
					success: function(res){

						try{
							var newjason = res;

							if( newjason.code === 'fail'){
								$(selector).data('_sh_like_it', false );
								wow_themes.loading(false);
								wow_themes.msg( newjason.msg, 'error' );
							}else if( newjason.code === 'success' ){
								//$('a[data-id="'+options.data_id+'"]').html( '<i class="fa fa-heart-o"></i> '+newjason.value );
								wow_themes.loading(false);
								$(selector).data('_sh_like_it', true );
								wow_themes.msg( newjason.msg, 'success' );
							}
							
						}
						catch(e){
							wow_themes.loading(false);
							$(selector).data('_sh_like_it', false );
							wow_themes.msg( 'There was an error with request '+e.message, 'error' );
							
						}
					}
				});
			},
			loading: function( show ){
				if( $('.ajax-loading' ).length === 0 ) {
					$('body').append('<div class="ajax-loading" style="display:none;"></div>');
				}
				
				if( show === true ){
					$('.ajax-loading').show('slow');
				}
				if( show === false ){
					$('.ajax-loading').hide('slow');
				}
			},
			
			msg: function( msg, type ){
				if( $('#pop' ).length === 0 ) {
					$('body').append('<div style="display: none;" id="pop"><div class="pop"><div class="alert"><p></p></div></div></div>');
				}
				if( type === 'error' ) {
					type = 'danger';
				}
				var alert_type = 'alert-' + type;
				
				$('#pop > .pop p').html( msg );
				$('#pop > .pop > .alert').addClass(alert_type);
				
				$('#pop').slideDown('slow').delay(5000).fadeOut('slow', function(){
					$('#pop .pop .alert').removeClass(alert_type);
				});
				
				
			},
			
		};
	
	$.fn.tweets = function( options ){
		
		var settings = {
				screen_name	:	'wordpress',
				count		:	3,
				template	:	'blockquote'
			};
			
			options = $.extend( settings, options );
			
			wow_themes.tweets( options, this );
			
			
	};
	
	$(document).ready(function() {
        
		$('.add_to_wishlist, a[rel="product_del_wishlist"]').click(function(e) {
			e.preventDefault();
			
			if( $(this).attr('rel') === 'product_del_wishlist' ){
				if( confirm( 'Are you sure! you want to delete it' ) ){
					var opt = {subaction:'wishlist_del', data_id:$(this).attr('data-id')};
					wow_themes.wishlist( opt, this );
				}
			}else{
				var opt = {subaction:'wishlist', data_id:$(this).attr('data-id')};
				wow_themes.wishlist( opt, this );
			}
			
		});/**wishlist end*/
		
		$('.jolly_like_it').click(function(e) {
			e.preventDefault();
			
				var opt = {subaction:'likeit', data_id:$(this).attr('data-id')};
				wow_themes.wishlist( opt, this );
			
			
		});/**wishlist end*/
		
		$('span.cart_quentity_plus, span.cart_quentity_minus').click(function(e) {
            e.preventDefault();
			
			var field = $('input[data-rel="quantity"]', $(this).parents('.shop_meta'));//$('input[name="quantity"]');
			console.log(field);
			var value = parseInt(field.val());
			console.log(value);
			var newval = 0;
			if( $(this).hasClass('cart_quentity_plus') ) {
				newval = value + 1;
				field.val(newval);
			}
			else if( $(this).hasClass('cart_quentity_minus') ) {
				if( value > 1 ) {
					newval = value - 1;
					field.val(newval);
				}
			}
			
			
        });
		
		/*$('body').bind('added_to_cart', function( frag, hash ) {
			//console.log( hash );
			$('body').append('<div id="sh_added_to_cart"></div>');
			$('div#sh_added_to_cart').html('Product Added');//.prettyPhoto();
			//$.prettyPhoto.open('Title','Description');
			//jQuery("div#sh_added_to_cart").prettyPhoto().open();
		});*/
		
		$('#contactform').submit(function(){

			var action = $(this).attr('action');
	
			$("#message").slideUp(750,function() {
			$('#message').hide();
	
			$('button#submit').attr('disabled','disabled');
			$('img.loader').css('visibility', 'visible');
	
			$.post(action, {
				contact_name: $('#contact_name').val(),
				contact_email: $('#email_address').val(),
				contact_website: $('#website').val(),
				contact_subject: $('#subject').val(),
				contact_message: $('#comments').val(),
				verify: $('#verify').val()
			},
				function(data){
					document.getElementById('message').innerHTML = data;
					$('#message').slideDown('slow');
					$('#contactform img.loader').css('visibility', 'hidden' );
					
					$('#submit').removeAttr('disabled');
					if(data.match('success') != null) $('#contactform').slideUp('slow');
	
				}
			);

		});

		return false;

	});
		
    });/** document.ready end */
		
})(jQuery);