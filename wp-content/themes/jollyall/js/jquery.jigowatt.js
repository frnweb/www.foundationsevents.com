jQuery(document).ready(function($){

	$('#contactform').submit(function(){

		var action = $(this).attr('action');
	
		$("#respond_message").slideUp(750,function() {
		$('#respond_message').hide();

		$('button#contact_submit').attr('disabled','disabled');
		$('img.form_loader').css('visibility', 'visible');

		$.post(action, {
				contact_name: $('#contact_name').val(),
				contact_email: $('#contact_email').val(),
				contact_website: $('#contact_website').val(),
				contact_subject: $('#contact_subject').val(),
				contact_message: $('#contact_message').val(),
				verify: $('#verify').val()
			},
			function(data){
				document.getElementById('respond_message').innerHTML = data;
				$('#respond_message').slideDown('slow');
				$('#contactform img.form_loader').css('visibility', 'hidden' );
				
				$('#contact_submit').removeAttr('disabled');
				if(data.match('success') != null) $('#contactform').slideUp('slow');

			}
		);

		});

		return false;

	});
	
	
	$('#contactform_1').submit(function(){

		var action = $(this).attr('action');
	
		$("#respond_message_1").slideUp(750,function() {
		$('#respond_message_1').hide();

		$('button#contact_submit_1').attr('disabled','disabled');
		$('img.form_loader_1').css('visibility', 'visible');

		$.post(action, {
				contact_name: $('#contact_name_1').val(),
				contact_email: $('#contact_email_1').val(),
				contact_subject: $('#contactsubject_1').val(),
				contact_message: $('#contact_message_1').val(),
			},
			function(data){
				document.getElementById('respond_message_1').innerHTML = data;
				$('#respond_message_1').slideDown('slow');
				$('#contactform_1 img.form_loader_1').css('visibility', 'hidden' );
				
				$('#contact_submit_1').removeAttr('disabled');
				if(data.match('success') != null) $('#contactform_1').slideUp('slow');

			}
		);

		});

		return false;

	});
	
	
	$('#contactform1').submit(function(){

		var action = $(this).attr('action');
	
		$("#respond_message1").slideUp(750,function() {
		$('#respond_message1').hide();

		$('button#contact_submit1').attr('disabled','disabled');
		$('img.form_loader1').css('visibility', 'visible');

		$.post(action, {
				contact_name: $('#contact_name1').val(),
				contact_email: $('#contact_email1').val(),
				contact_subject: $('#contact_website1').val(),
				contact_message: $('#contact_message1').val(),
			},
			function(data){
				document.getElementById('respond_message1').innerHTML = data;
				$('#respond_message1').slideDown('slow');
				$('#contactform1 img.form_loader1').css('visibility', 'hidden' );
				
				$('#contact_submit1').removeAttr('disabled');
				if(data.match('success') != null) $('#contactform1').slideUp('slow');

			}
		);

		});

		return false;

	});

});