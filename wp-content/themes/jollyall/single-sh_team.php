<?php

get_header(); 

$settings  = _WSH()->get_meta();

$meta = _WSH()->get_meta('_sh_layout_settings');

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'default-sidebar' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-8 col-md-8';

?>

<?php get_template_part( 'includes/modules/header/header', 'single' ); ?>

<?php while( have_posts() ): the_post(); 
	
	$meta = _WSH()->get_meta();?>

	<section class="white-wrapper">
		<div class="container">
			<div class="module clearfix">
				<div class="about-wrapper">
					<div class="col-ld-6 col-md-6 col-sm-6 col-xs-12">
						<div class="about_img">
							<?php the_post_thumbnail('600x600', array('class' => 'img-responsive' ) ); ?>
							<div class="title">
								<h3><?php the_title(); ?> - <?php echo sh_set( $meta, 'designation' ); ?></h3>
							</div><!-- end title -->
						</div><!-- end about img -->
					</div><!-- end col -->
	
					<div class="col-ld-6 col-md-6 col-sm-6 col-xs-12">
						<div class="module_widget">
							<div class="title">
								<h3><?php _e('What I Do?', SH_NAME); ?></h3>
							</div><!-- end title -->
							<div class="desc">
								<?php the_content(); ?>
							</div><!-- end desc -->
							
							<?php if( $services = sh_set( $meta, 'services' ) ) foreach( $services as $serv ): 
							
								$ser_post = get_post( $serv );
								if( !$ser_post ) continue;
								
								$ser_meta = _WSH()->get_meta('_sh_sh_services_settings', $ser_post->ID); ?>
							
								<div itemscope itemtype="https://schema.org/Service" class="service-box black-edition">
									<div class="icon-container">
										<i class="<?php echo sh_set( $ser_meta, 'fontawesome' ); ?>"></i>
									</div><!-- icon-container -->
									<div class="title">
									
										<?php if( $link = sh_set( $ser_meta, 'link' ) ): ?>
										<h3 itemprop="name"><a itemprop="url" href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( get_the_title( $ser_post->ID ) ); ?>"><?php echo get_the_title( $ser_post->ID ); ?></a></h3>
										<?php else: ?>
										<h3 itemprop="name"><?php echo get_the_title( $ser_post->ID ); ?></h3>
										<?php endif; ?>
	
									</div><!-- end title -->
									<div itemprop="text" class="desc">
										<?php echo _sh_trim( $ser_post->post_content, 15 ); ?>
									</div><!-- end desc -->
								</div><!-- end service-box -->
							
							<?php endforeach; ?>
							
						
						</div><!-- end module widget -->
					</div><!-- end col -->
				</div><!-- end about-wrapper -->
			</div><!-- end module -->
		</div><!-- end container -->
	</section>
	
	
	<section class="halfscreen parallax" style="background-image: url('<?php echo sh_set( $meta, 'skill_bg' ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay dark-version">
        	<div class="container">
            	<div class="module clearfix">
                    <div class="title wow zoomIn clearfix">
                        <h2><?php echo sh_set( $meta, 'skills_title' ) ? sh_set( $meta, 'skills_title' ) : __( 'My Skills', SH_NAME ); ?></h2>
                        <hr>
                    </div><!-- end title -->
					
					<?php $ex_skills = explode( "\n", sh_set( $meta, 'skills' ) ); ?>
					
					
                    <div class="row text-center">
						
						<?php if( $ex_skills ) foreach( $ex_skills as $ex_sk ): 
							$ex_sk_each = explode( '|', $ex_sk );
							if( !$ex_sk_each ) continue; ?>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="skill">
									<span class="chart" data-bar-color="#fff" data-percent="<?php echo (int)$ex_sk_each[1];?>"><span class="percent"></span></span>
									<h3><?php echo $ex_sk_each[0];?></h3>
								</div>
							</div>
						<?php endforeach; ?>
						
                    </div><!-- end row -->
            
                </div><!-- end module -->
            </div><!-- end container -->
    	</div><!-- end overlay -->
    </section><!-- end transparent-bg -->
	<script type="text/javascript" src="<?php echo  get_template_directory_uri(). '/js/jquery.easypiechart.min.js'; ?>"></script> 
	
<?php endwhile; ?>

<?php get_footer(); ?>

