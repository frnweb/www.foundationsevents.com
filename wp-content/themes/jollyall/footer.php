
<?php $options = get_option(SH_NAME.'_theme_options'); 
$boxed = sh_set( $options, 'boxed_layout' );
if( is_page() ) {
	$meta = _WSH()->get_meta();//printr($meta);
	$boxed = sh_set( $meta, 'boxed' ) ? true : $boxed;
}?>
<?php if( $boxed ): ?>
</div>
<?php endif; ?>

<div class="clearfix"></div>

<?php if( sh_set( $options, 'show_footer_1' ) || sh_set( $options, 'show_footer_2' )  ): ?>
	<footer id="footer-1" class="footer">
		<div class="container">
			<div class="clearfix">
			<?php if( sh_set( $options, 'show_footer_1' ) ): ?>
				<?php dynamic_sidebar( 'footer-sidebar' ); ?>
			<?php endif; ?>
			<?php if( sh_set( $options, 'show_footer_1' ) && sh_set( $options, 'show_footer_2' )  ): ?>
				<div class="clearfix"></div>
				<hr>
			<?php endif; ?>	
			<?php if( sh_set( $options, 'show_footer_2' ) ): ?>	
				<?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
				<?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
			<?php endif; ?>	
			</div>
		</div>
	</footer><!-- FOOTER -->
<?php endif; ?>

<section id="copyrights">
	<div class="container">
		<div class="col-lg-5 col-md-6 col-sm-12">
			<div class="copyright-text">
				<p><?php echo sh_set( $options, 'copyright_text' ); ?></p>
			</div><!-- end copyright-text -->
		</div><!-- end widget -->
		<div class="col-lg-7 col-md-6 col-sm-12 clearfix">
			<div class="footer-menu">
				<?php wp_nav_menu( array( 'container' => false, 'fallback_cb' => false, 'theme_location' => 'footer_menu' ) ); ?>
			</div>
		</div><!-- end large-7 --> 
	</div><!-- end container -->
</section>

<div class="dmtop"><?php _e('Scroll to Top', SH_NAME); ?></div>
	<!-- SCRIPTS-->

<?php //get_template_part('content', 'product_dropdown'); ?>

<?php wp_footer(); ?>
</div>
</body>

</html>