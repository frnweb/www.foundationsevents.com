
							<?php $meta = _WSH()->get_meta();
							$meta['size'] = '880x488'; ?>
							
                        	<div itemscope itemtype="http://schema.org/BlogPosting" class="blog-item clearfix">
                            	<div class="col-md-5">
                                    <div class="media-element entry">
                                        <?php echo sh_get_post_format_output($meta); ?>
                                    </div><!-- end media -->
                                </div><!-- end col -->
                                <div class="col-md-7">
                                    <div class="title">
                                        <h3 itemprop="headline">
											<a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
												<?php the_title(); ?>
											</a>
										</h3>
                                    </div><!-- end title -->
                                    <div class="meta">
										<span itemprop="datePublished"><i class="fa fa-calendar"></i> <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time></span> 		
										
										<span itemprop="author">
											<i class="fa fa-user"></i>
											<a rel="author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php the_author_meta( 'display_name' ); ?>"><?php the_author(); ?></a>
										</span>		
										
										<span><a href="javascript:void(0);"><i class="fa fa-heart-o"></i> <?php echo _WSH()->post_views(); ?></a></span>		
										<span itemprop="discussionUrl"><a href="<?php the_permalink(); ?>#comments"><i class="fa fa-comment"></i> <?php comments_number(); ?></a></span> 
                                        
                                    </div><!-- end meta -->
                                    
									<div itemprop="articleBody" class="desc">
										<?php the_excerpt(); ?>
									</div><!-- end desc -->
									
                                    <div class="clearfix">
										<a itemprop="url" href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>" class="btn btn-primary btn-dark btn-sm">
											<?php _e('Read More', SH_NAME); ?>
										</a>
									</div>
									
                                </div><!-- end col -->
                            </div><!-- end blog-item -->
