<?php

get_header(); 

$meta = _WSH()->get_term_meta( '_sh_category_settings' );

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'blog-sidebar' );
$view = sh_set( $meta, 'view', 'list' ) ? sh_set( $meta, 'view', 'list' ) : 'list';
_WSH()->page_settings = array('layout'=>$layout, 'view'=> $view, 'sidebar'=>$sidebar);

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-8 col-md-8';


?>
<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
		
				<div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="post-content">
				
					<?php if( have_posts() ):
						
						while( have_posts() ): the_post(); ?>
								
													
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								
								<?php if( $view == 2 ) get_template_part( 'blog', 'style' );
								else get_template_part( 'blog' ); ?>
								<!-- end blog-item -->
							</article>
		
						<?php endwhile; ?>
							
						<div class="clearfix"></div>
						
						<?php _the_pagination(); ?>
					
					<?php else: ?>
					
						<p><?php _e( 'Sorry, but nothing found on this page. Perhaps search may help you.', SH_NAME ); ?></p>
						<?php get_search_form(); ?>
						
					<?php endif; ?>
					
				</div>
        
				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
			</div>
    	</div>
    </div>
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>

<?php get_footer(); ?>


