<?php get_header(); ?>

<?php get_template_part('includes/modules/header/header', 'single' ); ?>


	
	<section class="white-wrapper">
    	<div class="container">
        	
			<div class="content-module">
				
				<div class="content halfwidth">
					<div class="general-row clearfix">
					
						<div class="row">
							<div itemscope itemtype="http://schema.org/CreativeWork">
							
								<?php while( have_posts() ): the_post(); 
									
									$meta = _WSH()->get_meta(); //printr($meta);
									_WSH()->meta = $meta;
									
									$is_full = sh_set( $meta, 'full' ) ? true : false;?>
									
									<?php if( $is_full ) : ?>
										<div class="single-portfolio blog-item">
											<div class="media-object portfolio-item-second">
	
												<?php get_template_part( 'project', 'img' ); ?>
	
											</div><!-- end media -->
										</div>
									<?php endif; ?>
								
									<?php if( $is_full ) get_template_part( 'project', 'full' );
									else get_template_part( 'project', 'sidebar' ); ?>
								
								<?php endwhile; ?>
							
							</div>
						</div><!-- end row --> 
						
					</div>
				</div>
				
			</div>   
		</div><!-- end container -->
    </section>
	
	<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' );  ?>

<?php get_footer(); ?>

