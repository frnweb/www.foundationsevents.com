<?php $options = _WSH()->option(); 
$custom_header = sh_set( $options, 'custom_header' );
$custom_header = sh_set( $_GET, 'custom_header' ) ? 'center_logo' : 'default';

/** Set the default values */
$bg = sh_set( $options, 'archive_page_header_img' );
$title = sh_set( $options, 'archive_page_title' ) ;
$text = sh_set( $options, 'archive_page_header_text' );?>

<?php if( is_category() || is_tag() || is_tax( 'product_cat' ) ) {
	$meta = _WSH()->get_term_meta( '_sh_category_settings' );
	$object = get_queried_object();
	$bg = sh_set( $meta, 'header_img' );
	$title = ( sh_set( $meta, 'header_title' ) ) ? sh_set( $meta, 'header_title' ) : $object->name ;
	$text = sh_set( $meta, 'header_text' );
} else if ( is_search() ) {

	$bg = sh_set( $options, 'search_page_header_img' );
	$title = ( sh_set( $options, 'search_page_title' ) ) ? sh_set( $options, 'search_page_title' ) : get_search_query() ;
	$text = sh_set( $options, 'search_page_header_text' );

} else if( is_author() ) {
	$bg = sh_set( $options, 'author_page_header_img' );
	$title = ( sh_set( $options, 'author_page_title' ) ) ? sh_set( $options, 'author_page_title' ) : get_queried_object()->data->dispaly_name ;
	$text = sh_set( $options, 'author_page_header_text' );
}?>


<?php if( $custom_header == 'center_logo' ): ?>
	
	<section class="dark-wrapper" id="center_logo">
		<div class="light-overlay overlay dark-version">
        	<div class="page-header-wrap">
				<div class="container">
                    <div class="module">
                        <div class="title">
							<h2><span><?php echo $title; ?></span></h2>
							<?php if( $text ): ?>
							<p class="lead"><?php echo $text; ?></p>
							<?php endif; ?>
                        </div>
                    </div><!-- end module -->
                </div><!-- end container -->
            </div><!-- end page-header-wrap -->
        </div><!-- end overlay -->
    </section>

<?php else: ?>


<section data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6" style="background-image: url('<?php echo $bg; ?>');" class="halfscreen parallax">
	<div class="light-overlay overlay dark-version">
		<div class="page-header-wrap">
			<div class="container">
				<div class="module">
					<div class="title">
						<h2><span><?php echo $title; ?></span></h2>
						
						<?php if( $text ): ?>
						<p class="lead"><?php echo $text; ?></p>
						<?php endif; ?>
					
					</div>
				</div><!-- end module -->
			</div><!-- end container -->
		</div><!-- end page-header-wrap -->
	</div><!-- end overlay -->
</section>

<?php endif; ?>