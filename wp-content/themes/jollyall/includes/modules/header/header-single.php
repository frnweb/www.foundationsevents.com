<?php $options = _WSH()->option();
$custom_header = sh_set( $options, 'custom_header' );
$custom_header = sh_set( $_GET, 'custom_header' ) ? 'center_logo' : 'default';

$post_id = get_the_id();


if( class_exists('woocommerce') && is_shop() ) $post_id = get_option( 'woocommerce_shop_page_id' );
$meta = _WSH()->get_meta('_sh_header_settings', $post_id ); 
$bg_image = sh_set( $meta, 'bg_image' ) ? sh_set( $meta, 'bg_image' ) : 'http://jollythemes.com/html/jollyfolio/demos/top_head_07.png';
$title = ( sh_set( $meta, 'header_title' ) ) ? sh_set( $meta, 'header_title' ) : get_the_title();?>

<?php if( $custom_header == 'center_logo' ): ?>
	
	<section class="dark-wrapper" id="center_logo">
		<div class="light-overlay overlay dark-version">
        	<div class="page-header-wrap">
				<div class="container">
                    <div class="module">
                        <div class="title">
							<h2><span><?php echo $title; ?></span></h2>
							<?php if( $tagline = sh_set( $meta, 'tagline' ) ): ?>
							<p class="lead"><?php echo $tagline; ?></p>
							<?php endif; ?>
                        </div>
                    </div><!-- end module -->
                </div><!-- end container -->
            </div><!-- end page-header-wrap -->
        </div><!-- end overlay -->
    </section>

<?php else: ?>

<section data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6" style="background-image: url('<?php echo $bg_image; ?>');" class="halfscreen parallax">
	<div class="light-overlay overlay dark-version">
		<div class="page-header-wrap">
			<div class="container">
				<div class="module">
					<div class="title">
						<h2><span><?php echo $title; ?></span></h2>
						
						<?php if( $tagline = sh_set( $meta, 'tagline' ) ): ?>
						<p class="lead"><?php echo $tagline; ?></p>
						<?php endif; ?>
					
					</div>
				</div><!-- end module -->
			</div><!-- end container -->
		</div><!-- end page-header-wrap -->
	</div><!-- end overlay -->
</section>

<?php endif; ?>