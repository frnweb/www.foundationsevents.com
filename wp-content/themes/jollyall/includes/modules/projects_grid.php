
						
						
                            <div itemscope itemtype="http://schema.org/CreativeWork" class="classic-portfolio">
                                <div class="portfolio-item-second entry">
                                    
									<?php the_post_thumbnail( '600x489', array('class'=>'img-responsive') ); ?>

									<div class="magnifier">
                                        <div class="buttons">
                                            <span><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><i class="fa fa-link"></i></a></span>
                                            <span><a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" data-gal="prettyPhoto[product-gallery]"><i class="fa fa-search"></i></a></span>                                     
                                        </div><!-- end buttons -->
                                    </div><!-- end magnifier -->
                                
								</div><!-- end portfolio-item -->
                                
								<div class="title">
                                    <h4 itemprop="name"><?php the_title(); ?></h4>
                                </div><!-- end title -->
                                
								<div class="desc" itemprop="text">
                                    <?php echo _sh_trim( get_the_content(), $limit ); ?>
                                </div><!-- end desc -->
                            
							</div><!-- end classic-portfolio -->
                        