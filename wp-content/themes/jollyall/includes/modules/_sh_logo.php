<?php $options = _WSH()->option();

$custom_header = sh_set( $_GET, 'custom_header' ) ? get_template_directory_uri().'/images/dark_logo.png' : sh_set( $options, 'logo_image');

$logo_type = sh_set( $options, 'logo_type', 'image' );
if( $logo_type == 'text' )
{
  //printr($theme_options);
  $LogoStyle = sh_get_font_settings( array( 'logo_font_size' => 'font-size', 'logo_font_face' => 'font-family', 'logo_font_style' => 'font-style', 'logo_color' => 'color' ), ' style="', '"' );
  $Logo = sh_set( $options, 'logo_heading', get_bloginfo('name'));
}
else
{
  $LogoStyle = '';
  $LogoImageStyle = ( sh_set( $options, 'logo_width' ) || sh_set( $options, 'logo_height' ) ) ? ' style="': '';
  $LogoImageStyle .= ( sh_set( $options, 'logo_width' ) ) ? ' width:'.sh_set( $options, 'logo_width' ).'px;': '';
  $LogoImageStyle .= ( sh_set( $options, 'logo_height' ) ) ? ' height:'.sh_set( $options, 'logo_height' ).'px;': '';
  $LogoImageStyle .= ( sh_set( $options, 'logo_width' ) || sh_set( $options, 'logo_height' ) ) ? '"': '';
  $Logo = '<img src="'.$custom_header.'" alt="'.get_bloginfo('name').'"'.$LogoImageStyle.' />';
}
?>


<a id="brand" class="pulse navbar-brand" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"<?php echo $LogoStyle;?>>
  <?php echo $Logo;?>
  <?php if( $slogan_heading = sh_set( $options, 'slogan_heading' ) ): ?>
	<?php if( $logo_type == 'text' ): ?>
		<p><?php echo $slogan_heading; ?></p>
	<?php endif; ?>
  <?php endif; ?>
</a>