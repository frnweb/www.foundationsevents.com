<?php ob_start(); ?>

<?php if( $bg ): ?>
<section class="halfscreen parallax" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
<?php else: ?>
<section class="no-parallax">
<?php endif; ?>

	<div class="<?php echo ( $bg ) ? 'overlay dark-version' : 'no-overlay'; ?>">
		<div class="container">
		
			<div class="module">
				<div class="about_message text-center clearfix">
					<h2>&ldquo; <?php echo $heading; ?>&rdquo;</h2>
				</div><!-- end welcome_message -->
			</div><!-- end module -->

			
		</div><!-- end container -->
	</div><!-- end overlay -->
</section>

<?php return ob_get_clean(); ?>

