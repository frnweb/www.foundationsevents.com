<?php 
$query_args = array('post_type' => 'sh_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['testimonials_category'] = $cat;

$query = new WP_Query($query_args) ; 

ob_start();?>


	<div class="grey-wrapper">
		<div class="container">
			<div class="module clearfix">
				
				<?php if( $title ): ?>
					<div class="title wow zoomIn text-center clearfix">
						<h2><?php echo $title; ?></h2>
						<hr>
					</div><!-- end parallax-title -->
				<?php endif; ?>
				
                <div class="testi_list row clearfix">
                    
					<?php while( $query->have_posts() ): $query->the_post(); 
						
						$meta = _WSH()->get_meta(); ?>
					
						<div class="col-md-4">
							<div class="testi_container wow bounceInDown clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								
									<?php the_post_thumbnail( 'thumbnail', array('class'=>'img-responsive img-circle') ); ?>

								</div><!-- end col -->
								
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
									<div class="item-title">
										<h3><?php the_title(); ?></h3>
										<small><?php echo str_replace( 'http://', '', sh_set( $meta, 'link' ) ); ?></small>
									</div><!-- end title -->
									<div class="desc">
										<p>&ldquo; <?php echo _sh_trim( get_the_content(), $limit ); ?>&rdquo; </p>
									</div><!-- end desc -->
								</div><!-- end col -->
							</div><!-- end testi_container -->
						</div><!-- end col-6 -->
                	
					<?php endwhile; ?>
					
                </div><!-- end team_list -->
			</div><!-- end module --> 
		</div><!-- end container -->
    </div><!-- end white-wrapper-bg -->


<?php wp_reset_query();
return ob_get_clean();
