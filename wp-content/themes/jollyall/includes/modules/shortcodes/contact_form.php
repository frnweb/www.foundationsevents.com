<?php wp_enqueue_script( array( 'jquery-jigowatt' ) );
ob_start(); ?>

<div class="white-wrapper module">

	<div class="container">
	
		<?php if( $title ): ?>
		
			<div class="title wow zoomIn text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div>
		
		<?php endif; ?>
		
		<div class="contact_form clearfix">
		
			<div class="col-md-12"><div id="respond_message"></div></div>
			<form id="contactform" action="<?php echo admin_url( 'admin-ajax.php?action=_sh_ajax_callback&amp;subaction=contact_form_submit'); ?>" name="contactform" method="post">
			
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<input type="text" required="required" name="contact_name" id="contact_name" class="form-control" placeholder="<?php _e('Name', SH_NAME); ?>"> 
					<input type="text" required="required" name="contact_email" id="contact_email" class="form-control" placeholder="<?php _e('Email Address', SH_NAME); ?>"> 
					<input type="text" name="contact_website" id="contact_website" class="form-control" placeholder="<?php _e('Website', SH_NAME); ?>"> 
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="contact_subject" id="contact_subject" class="form-control" placeholder="<?php _e('Subject', SH_NAME); ?>"> 
					<textarea class="form-control" name="contact_message" id="contact_message" rows="6" placeholder="<?php _e('Message', SH_NAME); ?>"></textarea>
					<button type="submit" value="<?php _e('SEND', SH_NAME); ?>" id="contact_submit" class="btn btn-lg btn-primary pull-right"><?php _e('Send', SH_NAME); ?></button>
					<img class="form_loader" alt="" src="<?php echo get_template_directory_uri();?>/images/ajaxLoader.gif" style="visibility:hidden;">
				</div>
			</form>    
		</div>
	</div>

</div>

<?php return ob_get_clean(); ?>