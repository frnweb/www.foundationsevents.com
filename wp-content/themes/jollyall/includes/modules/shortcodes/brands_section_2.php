<?php $count = 1; 
wp_enqueue_script( array( 'owl.carousel.min' ) ) ;
$options = _WSH()->option();
$get_brands = sh_set(sh_set($options , 'brand_or_client') , 'brand_or_client' ) ;

if( !$get_brands ) return;

/** remove extra key tocopy */

foreach( $get_brands as $k => $v ) {
	if( sh_set( $v, 'tocopy' ) ) unset( $get_brands[$k] );
}


$get_brands = ( $order == 'DESC' ) ? array_reverse( (array)$get_brands ) : $get_brands;

$brands = ( $num ) ? array_slice( $get_brands, 0, ( $num ) ) : $get_brands;
$col_md = ( $cols == 3 ) ? 'col-md-4' : 'col-md-3';

ob_start();?>

				<div class="container">
					<div class="client-module">
                        <div class="clearfix">
                        	
							
							<?php foreach( $brands as $c ): ?>
							
								<div class="<?php echo $col_md; ?>">
									<a href="<?php echo esc_url(sh_set($c , 'link')); ?>" title="<?php echo esc_attr(sh_set($c , 'title')); ?>" target="_blank">
										<div class="client-wrap">
											<img class="img-responsive" alt="<?php echo esc_attr(sh_set($c , 'title')); ?>" src="<?php echo esc_url(sh_set($c , 'brand_img')); ?>">
										</div><!-- end client -->
									</a>
								</div>
								
							
							<?php endforeach; ?>
							
                        </div><!-- end row -->
                    </div>
				</div>

<?php return ob_get_clean(); ?>
