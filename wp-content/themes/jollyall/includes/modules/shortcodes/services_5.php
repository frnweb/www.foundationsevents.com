<?php $query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['services_category'] = $cat;

$query = new WP_Query($query_args) ; 
$count = 1 ;
ob_start();?>

<?php if( $bg ): ?>
<div class="halfscreen parallax" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
	<div class="overlay dark-version">
<?php else: ?>
<div class="white-wrapper clearfix">
<?php endif; ?>

	<div class="container">
	
		<div class="module">
			
			<?php if( $title ): ?>
			
			<div class="title wow zoomIn text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
				<p class="lead"><?php echo $slogan; ?></p>
			</div><!-- end title -->
			
			<?php endif; ?>

			<div class="row service-list-vertiical">
				
				<?php while( $query->have_posts() ): $query->the_post();
				$meta = _WSH()->get_meta(); ?>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div itemscope itemtype="https://schema.org/Service" class="service-box black-edition">
						
						<div class="icon-container">
							<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
						</div><!-- icon-container -->
						
						<div class="title">
							<?php if( $link = sh_set( $meta, 'link' ) ): ?>
								<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							<?php else: ?>
								<h3 itemprop="name"><?php the_title(); ?></h3>
							<?php endif; ?>
						</div><!-- end title -->
						<div class="desc">
							<p itemprop="text"><?php echo _sh_trim( get_the_content(), $limit ); ?></p>
						</div><!-- end desc -->
					</div><!-- end service-box -->
				</div><!-- end col -->
				
				<?php endwhile; ?>
			
			</div><!-- end row -->
		</div><!-- end module --> 
	</div><!-- end container -->
</div><!-- end white-wrapper -->

<?php if( $bg ): ?>
</div>
<?php endif; ?>

<?php return ob_get_clean();

