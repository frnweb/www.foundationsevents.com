<?php  $query_args = array('post_type' => 'post' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order );
if( $cat ) $query_args['category_name'] = $cat;
$query = new WP_Query($query_args) ; 

$count = 1; 
wp_enqueue_script( array('owl.carousel.min', 'jquery-fitvids', 'jquery-prettyPhoto' ));

$cols = ( $cols ) ? $cols : 3;
$col_md = ( $cols == 2 ) ? 'col-md-6 col-sm-12' : 'col-md-4 col-sm-6';
$blog_wraper = ( $title ) ? 'blog-wrapper' : 'blog_wrappers';

ob_start();

?>


		<div class="module">
			
			<?php if( $title ): ?>
			
			<div class="title text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
			
			<?php endif; ?>
			
			<div class="<?php echo $blog_wraper; ?>">
				<div class="row">
					
					<?php while( $query->have_posts() ): $query->the_post(); 
					
					$limit = ( $limit ) ? $limit : 10; 
					$meta = _WSH()->get_meta(); 
					$meta['size'] = '880x488'; 
					$first_last = '';
					
					if( ( ( $query->current_post + 1 ) % $cols ) == 0 ) $first_last = ' last';
					elseif( $query->current_post == 0 || ( ( $query->current_post + $cols ) % $cols ) == 0 ) $first_last = ' first';?>
					
					<div class="<?php echo $col_md.$first_last; ?> col-xs-12">
						<div itemscope itemtype="http://schema.org/BlogPosting"  class="blog-item">
							<div class="media-element entry">
								
								<?php echo sh_get_post_format_output($meta); ?>
								
							</div><!-- end media -->
							<div class="title">
								<h3>
									<a itemprop="headline" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h3>
							</div><!-- end title -->
							<div class="meta">
								<span><a itemprop="datePublished" href="#"><i class="fa fa-calendar"></i> <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time></a></span>		
								
								<span>
									<a itemprop="author" rel="author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php the_author_meta( 'display_name' ); ?>"><i class="fa fa-user"></i> <?php the_author(); ?> </a>			
								</span>		
								
								<span>
									<a href="javascript:void(0);" class="jolly_like_it" data-id="<?php the_ID(); ?>"><i class="fa fa-heart-o"></i> <?php echo (int)get_post_meta( get_the_id(), '_jolly_like_it', true ); ?></a>
								</span>		
								<span><a itemprop="discussionUrl" href="<?php the_permalink(); ?>#comments" title="<?php the_title_attribute(); ?>"><i class="fa fa-comments"></i> <?php comments_number(0, 1, '%');?></a></span>		
							</div><!-- end meta -->
							<div itemprop="articleBody" class="desc">
								<?php echo _sh_trim( get_the_excerpt(), $limit ); ?>
							</div><!-- end desc -->
							<div class="clearfix">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-primary btn-dark btn-sm"><?php _e('Read More', SH_NAME); ?></a>	
							</div>
						</div><!-- end blog-item -->
					</div><!-- end col -->
					<?php endwhile; ?>
					
					
				</div><!-- end row -->
			</div><!-- end blog-wrap -->
		</div><!-- end module --> 
	

<?php wp_reset_query();
return ob_get_clean(); ?>
