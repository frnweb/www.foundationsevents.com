<?php ob_start(); ?>
					
					<div class="module-widget clearfix">
						
						<?php if( $title ): ?>
						<div class="title">
							<h3><?php echo $title; ?></h3>
						</div>
						<?php endif; ?>
						<?php $exp_images = explode( ',', $images ); ?>
						
						<?php if( $exp_images ): ?>
						
							<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
								<!-- Indicators -->
								
								<ol class="carousel-indicators">
									<?php foreach( $exp_images as $k=>$e_img ): 
										
										$active = ( $k == 0 ) ? ' class="active"' : '';?>
										<li data-target="#myCarousel" data-slide-to="<?php echo $k;?>"<?php echo $active; ?>></li>
									
									<?php endforeach; ?>
									
								</ol>
								<div class="carousel-inner">
								
									<?php foreach( $exp_images as $k=>$e_img ): 
										
										$active = ( $k == 0 ) ? ' active' : '';?>

										<div class="item<?php echo $active; ?>"> <?php echo wp_get_attachment_image( $e_img, 'full' ); ?> </div>
									<?php endforeach; ?>
									
								</div>
								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> 
								
							</div>		
							<!-- /.carousel --> 
						
						<?php endif; ?>
					</div>

<?php return ob_get_clean();
