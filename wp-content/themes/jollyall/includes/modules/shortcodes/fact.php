<?php ob_start(); ?>

							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="milestone-counter">
								
                                    <?php if( $icon ): ?>
									<i class="fa <?php echo $icon; ?> fa-3x"></i>
									<?php endif; ?>
									
									<span class="stat-count highlight"><?php echo $number; ?></span>
                                    <div class="milestone-details"><?php echo $title; ?></div>
                                </div>
                            </div>

<?php return ob_get_clean(); 
