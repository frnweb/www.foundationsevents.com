<?php 
$query_args = array('post_type' => 'sh_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
//if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'testimonials_category','field' => 'id','terms' => $cat));
if( $cat ) $query_args['testimonials_category'] = $cat;
$query = new WP_Query($query_args) ; 
$bg_src = wp_get_attachment_url($bg);
wp_enqueue_script( array( 'owl.carousel.min' ) ) ; 
ob_start();
?>

<?php if( $bg ): ?>
<div class="halfscreen parallax" style="background-image: url('<?php echo $bg_src; ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
	<div class="overlay dark-version">
<?php else: ?>
<div class="white-wrapper">
	<div class="white-testimonial">
<?php endif; ?>

		<div class="container">
			<div class="module clearfix">
				
				<?php if( $title ): ?>
					<div class="title clearfix">
						<h2><?php echo $title; ?></h2>
						<hr>
					</div><!-- end parallax-title -->
				<?php endif; ?>
				
				<div id="testimonial-carousel" class="owl-carousel text-left">
					
					<?php while( $query->have_posts() ): $query->the_post(); 
					$meta = _WSH()->get_meta();
					$limit = ( $limit ) ? $limit : 10; ?>
					
					<div class="item">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<?php the_post_thumbnail( 'thumbnail', array('class'=>'img-responsive' ) ); ?>
						</div><!-- end col -->
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="item-title">
								<h3><?php the_title(); ?></h3>
								<small><?php echo sh_set( $meta, 'designation' ); ?> @ <?php echo sh_set( $meta, 'company' ); ?></small>
							</div><!-- end title -->
							<div class="desc">
								<p>&ldquo;<?php echo _sh_trim( get_the_excerpt(), $limit ); ?>&rdquo;</p>
							</div><!-- end desc -->
						</div><!-- end col -->
					</div><!-- end item -->
					
					<?php endwhile; ?>
					
				</div><!-- end testimonial-carousel -->
			</div><!-- end module --> 
		</div><!-- end container -->
	</div><!-- end overlay -->
</div><!-- end transparent-bg -->
<?php wp_reset_query();
return ob_get_clean(); ?>
