<?php if( $rotating_text ) wp_enqueue_script( array('textrotate') );?>
<?php $img_src = sh_set(wp_get_attachment_image_src($bg , 'full') , 0); 
ob_start();

$bg_img = ( $bg ) ? ' style="background-image:url('.wp_get_attachment_url($bg).')"' : '';?>

<?php if( $center ): ?>
<div class="container">
<?php endif; ?>
<div data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6"<?php echo $bg_img; ?> class="halfscreen parallax">
	<?php $overlay_class = ( $overlay ) ? 'light-overlay' : 'overlay dark-version'; ?>
	<div class="<?php echo $overlay_class; ?>">
		<div class="container">
		
			<div class="module">
				
				<?php if( $title || $title_after || $rotating_text ): ?>
				
				<div class="title">
					<div class="rw-words rw-words-1">
						<h2>
							<?php echo $title; ?> 
							<?php if( $rotating_text ): ?>
							<span id="rotateme"><?php echo $rotating_text; ?></span> 
							<?php endif; ?>
							<?php echo $title_after; ?>
						</h2>
					</div>
				</div><!-- end parallax-title -->
				<?php endif; ?>
				
				<?php if( $image ): ?>
				<div class="text-center banner-img">
					<?php echo wp_get_attachment_image( $image, 'full', '', array('class'=>'img-responsive' ) ); ?>
				</div><!-- text-center -->
				<?php endif; ?>
				
				<div class="row">
					<div class="stat clearfix">
						<?php echo do_shortcode( $contents ); ?>
					</div><!-- stat -->
				</div><!-- end row -->
			</div><!-- end module --> 
		</div><!-- end container -->
	</div><!-- end overlay -->
</div>

<?php if( $center ): ?>
</div>
<?php endif; ?>

<?php return ob_get_clean(); ?>
