<?php ob_start(); 

$lat = ( $lat ) ? $lat : -37.801578;
$lang = ( $lang ) ? $lang : 145.060508;?>


<div id="map"></div>
<div class="clearfix"></div>

<!-- Google Map -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
	var locations = [
	['<div class="infobox"><?php echo str_replace( "\n", '', $contents ); ?></div>', <?php echo $lat; ?>, <?php echo $lang; ?>, 2]
	];

	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 10,
		scrollwheel: false,
		navigationControl: true,
		mapTypeControl: false,
		scaleControl: false,
		draggable: true,
		styles: [ { "stylers": [ { "hue": "#000" },  {saturation: -100},
			{gamma: 0.50} ] } ],
		center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lang; ?>),
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var infowindow = new google.maps.InfoWindow();

	var marker, i;

	for (i = 0; i < locations.length; i++) {  
  
		marker = new google.maps.Marker({ 
		position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
		map: map ,
		icon: '<?php echo get_template_directory_uri(); ?>/images/marker.png'
		});


	  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
		  infowindow.setContent(locations[i][0]);
		  infowindow.open(map, marker);
		}
	  })(marker, i));
	}
</script>


<?php return ob_get_clean();