<?php $terms = get_terms('product_cat', array('number' => $num )); 
//$chunk_size = round( (count( $terms ) / 3 ) );
//if( $chunk_size < 1 ) $chunk_size = 1;
if( !$terms ) return;

//$chunks = array_chunk( $terms, $chunk_size ); //printr($chunks);

ob_start();
?>

		<div class="container">
			<div class="module">
				<div class="row">
					
					<?php foreach( $terms as $t ): 
					
						$thumbnail_id = get_woocommerce_term_meta( $t->term_id, 'thumbnail_id', true ); ?>
                	
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="entry shop-banner">
								<div class="title">
									<h4>Spring 2014</h4>
									<hr>
									<h1><?php echo $t->name; ?></h1>
								</div>
								<?php echo wp_get_attachment_image( $thumbnail_id, '464x306', '', array('class'=>'img-responsive')); ?>
								
								<div class="banner-hover"></div>
							</div><!-- end entry -->
						</div><!-- end col -->
					
					<?php endforeach; ?>

                </div>
			</div>
		</div>



<?php return ob_get_clean();



