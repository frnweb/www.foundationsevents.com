<?php ob_start(); ?>

							<div class="module_widget clearfix">
                                <div class="title">
                                    <h3>Sales Charts</h3>
                                </div><!-- end title -->
                                <div class="desc">
                                    <p>Duis non lectus sit amet est imperdiet cursus ame elementum vitae eros. Etiam adipiscingmorbi vitae magna tellus.</p>
                                </div><!-- end desc -->
                                <div class="widget-main">
                                    <div id="sales-charts"></div>
                                </div>
                            </div><!-- end module_widget -->


<!-- Pie Chart Elements -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.resize.min.js"></script>
<script type="text/javascript">
      jQuery(function($) {  
        var data = [
        { label: "Green",  data: 30.7, color: "#48CA3B"},
        { label: "Blue",  data: 24.5, color: "#00BCE1"},
        { label: "Purple",  data: 10.2, color: "#4D3A7D"},
        { label: "Red",  data: 18.6, color: "#AD1D28"},
        { label: "Yellow",  data: 16, color: "#DEBB27"}
        ];
      

      
        var d1 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d1.push([i, Math.sin(i)]);
        }
      
        var d2 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d2.push([i, Math.cos(i)]);
        }
      
        var d3 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.2) {
          d3.push([i, Math.tan(i)]);
        }
      
        var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
        $.plot("#sales-charts", [
          { label: "Yellow", data: d1 },
          { label: "Blue", data: d2 },
          { label: "Red", data: d3 }
        ], {
          hoverable: true,
          shadowSize: 0,
          series: {
            lines: { show: true },
            points: { show: true }
          },
          xaxis: {
            tickLength: 0
          },
          yaxis: {
            ticks: 10,
            min: -2,
            max: 2,
            tickDecimals: 3
          },
          grid: {
            backgroundColor: { colors: [ "#fff", "#fff" ] },
            borderWidth: 1,
            borderColor:'#555'
          }
        });
      })
    </script>

<?php return ob_get_clean();