<?php wp_enqueue_script( array( 'owl.carousel.min' ) ) ;

$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 
ob_start();
?>

<div class="white-wrapper clearfix">
	<div class="container">
		<div class="text-center">
			
			<?php if( $title ): ?>
			<div class="title clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
			<?php endif; ?>

			<div id="services-carousel" class="owl-carousel text-left">
				
				<?php while( $query->have_posts() ): $query->the_post(); 
					$services_meta = _WSH()->get_meta();
					$even = ( $query->current_post % 2 );?>
				
					<div itemscope itemtype="https://schema.org/Service" class="item">
						
						<?php if( $even == 0 ): ?>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>
						</div><!-- end col -->
						<?php endif; ?>
						
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="item-title">
								<h3 itemprop="name"><?php the_title(); ?></h3>
							</div><!-- end title -->
							<div class="desc">
								<div itemprop="text">
									<?php echo _sh_trim( get_the_excerpt(), $limit ); ?>
								</div>
								<div class="button-wrapper">
									<?php if( $link = sh_set( $services_meta, 'link' ) ): ?>
										<a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-primary btn-dark"><?php _e('Read More', SH_NAME); ?></a> 
									<?php endif; ?>
									
									<?php if( $link = sh_set( $services_meta, 'hire_link' ) ): ?>
									<a href="<?php echo esc_url( $link ); ?>" title="" class="btn btn-primary btn-dark"><?php _e('Hire Us', SH_NAME); ?></a>
									<?php endif; ?>
								</div><!-- end button-wrapper -->
							</div><!-- end desc -->
						</div><!-- end col -->
						
						<?php if( $even !== 0 ): ?>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>
						</div><!-- end col -->
						<?php endif; ?>
						
					</div><!-- end item -->
				
				<?php endwhile; ?>
	
			</div><!-- end services-carousel -->
		</div><!-- end module --> 
	</div><!-- end container -->
</div><!-- end transparent-bg -->

<?php return ob_get_clean(); ?>
