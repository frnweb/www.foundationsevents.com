<?php 
$count = 1;
$query_args = array('post_type' => 'product' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['product_cat'] = $cat;

$query = new WP_Query($query_args) ; 
ob_start();?>

<div class="container">

	<?php if( $title ): ?>
	
		<div class="title wow zoomIn text-center clearfix">
			<h2><?php echo $title; ?></h2>
			<hr>
		</div><!-- end title -->
	
	<?php endif; ?>


	<div class="shop-wrap">
		<div class="row">
			
			<?php while ( $query->have_posts() ): $query->the_post(); 
			
				global $product;?>
			
				<?php $cart_button =
				apply_filters( 'woocommerce_loop_add_to_cart_link',
					 sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s"><i class="fa fa-shopping-cart"></i></a>',
					  esc_url( $product->add_to_cart_url() ),
					  esc_attr( $product->id ),
					  esc_attr( $product->get_sku() ),
					  $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
					  esc_attr( $product->product_type ),
					  esc_html( $product->add_to_cart_text() )
					 ),
				$product ); ?>
				
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="shop-item">
						<div class="media-element entry">
							
							<?php the_post_thumbnail( '265x372', array( 'class' => 'img-responsive' ) ); ?>
							
							<div class="magnifier">
								<div class="buttons">
									<span><?php echo $cart_button; ?></span>
									<span><a href="javascript:void(0);" class="add_to_wishlist" data-id="<?php the_ID(); ?>"><i class="fa fa-heart-o"></i></a></span>
									<span><a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id() ); ?>" data-gal="prettyPhoto[product-gallery]"><i class="fa fa-search"></i></a></span>                   
								</div><!-- end buttons -->
							</div><!-- end magnifier -->
						</div><!-- end media -->
						<div class="title">
							<h3>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_title(); ?>
								</a>
							</h3>
							<h4><?php woocommerce_template_loop_price(); ?> </h4>
						</div><!-- end title -->
					</div><!-- end blog-item -->
				</div><!-- end col -->
			
			<?php endwhile; ?>
		
		</div>
	</div>
	
</div>

<?php wp_reset_query();

return ob_get_clean(); ?>




