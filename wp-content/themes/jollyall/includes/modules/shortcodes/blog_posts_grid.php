<?php ob_start();
$paged = get_query_var('page');
$query_args = array('post_type' => 'post' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order , 'category' => $cat, 'paged' => $paged );
$query = new WP_Query($query_args) ; $count = 1;  
wp_enqueue_script( array('jquery.isotope.min', 'jquery-fitvids' )); 
_WSH()->page_settings = array('layout'=>'full', 'view'=> 'grid', 'sidebar'=>'');?>
<div class="title">
    <h2><?php echo $title; ?></h2>
</div>
                
<div class="row">
   <div class="blog-masonry">
    <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post();  global $post ; 
				 if($count == 1)  $class = 'first' ;
				 elseif($count == 2) $class = 'last' ;
			 ?>
        <div class="col-lg-6 <?php echo $class; ?>">
            <?php get_template_part( 'blog', get_post_format() ); ?> 
        </div>
 	<?php 
		$count = ($count == 2) ? 1 : $count+1 ;
		endwhile ; endif;  wp_reset_query(); 
	?>
    </div>   
    <div class="clearfix"></div>
    
	<?php if( _the_pagination( array('total'=>$query->max_num_pages), false ) ): ?>

		<hr>
		<div class="pagination_wrapper">
		   <?php _the_pagination( array('total'=>$query->max_num_pages) ) ; ?>
    	</div>

	<?php endif; ?>
</div>

<?php wp_reset_query();
$output = ob_get_contents();
ob_end_clean();
return $output;