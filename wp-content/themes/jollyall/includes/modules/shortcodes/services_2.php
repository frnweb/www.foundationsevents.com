<?php 
$count = 1;
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 
ob_start();?>

<div class="grey-wrapper clearfix">

	<div class="container">
		<div class="module">

			<div class="col-lg-6 col-md-6 col-xs-12">
				<div class="service-list clearfix">
					
					<?php if( $title ): ?>
					<div class="title">
						<h3><?php echo $title; ?></h3>
					</div><!-- end title -->
					<?php endif; ?>
					
					<?php while( $query->have_posts() ): $query->the_post(); 
						
						$services_meta = _WSH()->get_meta() ; 
						$limit = ( $limit ) ? $limit : 20;?>
						
						<div itemscope itemtype="https://schema.org/Service" class="service-box">
							
							<div class="icon-container">
								<i class="<?php echo sh_set($services_meta , 'fontawesome'); ?>"></i>
							</div><!-- icon-container -->
							
							<div class="title">
	
								<?php if( $link = sh_set( $services_meta, 'link' ) ): ?>
								<h3 itemprop="name"><a itemprop="url" href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<?php else: ?>
								<h3 itemprop="name"><?php the_title(); ?></h3>
								<?php endif; ?>
	
							</div><!-- end title -->
							
							<div class="desc">
								<p itemprop="text"><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
							</div><!-- end desc -->
						
						</div><!-- end service-box -->

					<?php endwhile; ?>

				</div><!-- end service-list -->
			</div><!-- end col -->
			
			<?php if( $image ): ?>
			<div class="col-lg-6 col-md-6 col-xs-12">
				<?php echo wp_get_attachment_image( $image, 'full', '', array('class'=>'img-responsive', 'itemprop'=>'image') ); ?>
			</div><!-- end col -->
			<?php endif; ?>
			
		</div><!-- end module -->
	</div><!-- end container -->
</div>

<?php wp_reset_query();
return ob_get_clean(); ?>
