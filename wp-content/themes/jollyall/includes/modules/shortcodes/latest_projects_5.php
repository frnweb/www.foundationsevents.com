<?php global $wp_query;
wp_enqueue_script( array( 'jquery-prettyPhoto' ) );
$cols = ( $cols ) ? $cols : 3 ;
$paged = get_query_var( 'paged' );
$query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);

if( $cat ) $query_args['projects_category'] = $cat;
$col_md = ( $cols == 4 ) ? 'col-lg-3 col-md-3 ' : 'col-lg-4 col-md-4 ';
query_posts($query_args) ; 

ob_start();?>

	<div class="white-wrapper clearfix">
		<div class="container">
			<div class="content-module">
           		<div class="content fullwidth">
                    <div class="masonry_wrapper margin-top clearfix">
                        
						<?php while( have_posts() ): the_post(); 
						
							$first_last = _WSH()->first_last($wp_query->current_post, $cols); ?>

							<div class="<?php echo $col_md; ?>col-sm-6 col-xs-12<?php echo $first_last; ?>">
								<?php include( _WSH()->includes('includes/modules/projects_grid.php') ); ?>
							</div>

						<?php endwhile; ?>
                        
                        
                    </div><!-- end masonry_wrapper -->           

                <div class="clearfix"></div>
                                                                    
                <div class="pagination_wrapper text-center">
                	<?php _the_pagination(); ?>
                </div>
                               
                </div><!-- end content -->
			</div><!-- end module --> 
		</div><!-- end container -->
    </div>


<?php wp_reset_query();
return ob_get_clean();