<?php 
$query_args = array('post_type' => 'sh_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
//if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'team_category','field' => 'id','terms' => $cat));
if( $cat ) $query_args['team_category'] = $cat;
$query = new WP_Query($query_args) ;
ob_start() ;?>


<div class="grey-wrapper">
	<div class="container">
		<div class="module clearfix">
			
			<?php if( $title ): ?>
			<div class="title wow zoomIn text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
			<?php endif; ?>

			<div class="team_list text-center">
				
				<?php while( $query->have_posts() ): $query->the_post(); 
					$meta = _WSH()->get_meta();?>
				
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="team_member">
						<div class="media-element entry">
							
							<?php the_post_thumbnail( '268x280', array( 'class'=>'img-thumbnail img-responsive' ) ); ?>
							
							<div class="magnifier">
								<div class="buttons">
									
									<?php foreach( (array)sh_set($meta, 'sh_team_social' ) as $social): ?>
										
										<?php if(sh_set($social , 'social_link')) {?>
										<span><a target="_blank" href="<?php echo esc_url(sh_set($social , 'social_link')); ?>"><i class="<?php echo sh_set($social , 'social_icon'); ?>"></i></a></span> 						
										<?php } ?>					
									
									<?php endforeach; ?>
									                  
								</div><!-- end buttons -->
							</div><!-- end magnifier -->
						
						</div><!-- end media -->
						<h3><?php the_title(); ?></h3>
						<h4><?php echo sh_set( $meta, 'designation' ); ?></h4>
						<p><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
					</div><!-- end team_member -->
				</div><!-- end col-lg-3 -->
				
				<?php endwhile; ?>
				
				
			</div><!-- end team_list -->
			 
		</div><!-- end module --> 
	</div><!-- end container -->
</div><!-- end grey-wrapper -->


<?php wp_reset_query();
return ob_get_clean(); 
