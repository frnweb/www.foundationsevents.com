<?php ob_start(); ?>

	<section class="white-wrapper clearfix">
		<div class="container">
			<div class="content-module">
           		<div class="content fullwidth">
 
					<div class="site__header">
						<div class="wrap">
							<span id="animationSandbox" style="display: block;"><h1 class="site__title mega"><a href="#" class="btn btn-primary">Hello friend</a></h1></span>
							<span class="beta subhead">animate.css is a bunch of cool, fun, and cross-browser animations for you to use in your projects. Great for emphasis, home pages, sliders, and general just-add-water-awesomeness.</span>
						</div>
                    </div><!-- /.div -->

                	<div class="clearfix text-center">
                        <div class="col-md-4">
                        </div>
                    	<div class="col-md-4">
                            <form>
                              <select class="form-control input--dropdown js--animations">
                                <optgroup label="Attention Seekers">
                                  <option value="bounce">bounce</option>
                                  <option value="flash">flash</option>
                                  <option value="pulse">pulse</option>
                                  <option value="rubberBand">rubberBand</option>
                                  <option value="shake">shake</option>
                                  <option value="swing">swing</option>
                                  <option value="tada">tada</option>
                                  <option value="wobble">wobble</option>
                                </optgroup>
                        
                                <optgroup label="Bouncing Entrances">
                                  <option value="bounceIn">bounceIn</option>
                                  <option value="bounceInDown">bounceInDown</option>
                                  <option value="bounceInLeft">bounceInLeft</option>
                                  <option value="bounceInRight">bounceInRight</option>
                                  <option value="bounceInUp">bounceInUp</option>
                                </optgroup>
                        
                                <optgroup label="Bouncing Exits">
                                  <option value="bounceOut">bounceOut</option>
                                  <option value="bounceOutDown">bounceOutDown</option>
                                  <option value="bounceOutLeft">bounceOutLeft</option>
                                  <option value="bounceOutRight">bounceOutRight</option>
                                  <option value="bounceOutUp">bounceOutUp</option>
                                </optgroup>
                        
                                <optgroup label="Fading Entrances">
                                  <option value="fadeIn">fadeIn</option>
                                  <option value="fadeInDown">fadeInDown</option>
                                  <option value="fadeInDownBig">fadeInDownBig</option>
                                  <option value="fadeInLeft">fadeInLeft</option>
                                  <option value="fadeInLeftBig">fadeInLeftBig</option>
                                  <option value="fadeInRight">fadeInRight</option>
                                  <option value="fadeInRightBig">fadeInRightBig</option>
                                  <option value="fadeInUp">fadeInUp</option>
                                  <option value="fadeInUpBig">fadeInUpBig</option>
                                </optgroup>
                        
                                <optgroup label="Fading Exits">
                                  <option value="fadeOut">fadeOut</option>
                                  <option value="fadeOutDown">fadeOutDown</option>
                                  <option value="fadeOutDownBig">fadeOutDownBig</option>
                                  <option value="fadeOutLeft">fadeOutLeft</option>
                                  <option value="fadeOutLeftBig">fadeOutLeftBig</option>
                                  <option value="fadeOutRight">fadeOutRight</option>
                                  <option value="fadeOutRightBig">fadeOutRightBig</option>
                                  <option value="fadeOutUp">fadeOutUp</option>
                                  <option value="fadeOutUpBig">fadeOutUpBig</option>
                                </optgroup>
                        
                                <optgroup label="Flippers">
                                  <option value="flip">flip</option>
                                  <option value="flipInX">flipInX</option>
                                  <option value="flipInY">flipInY</option>
                                  <option value="flipOutX">flipOutX</option>
                                  <option value="flipOutY">flipOutY</option>
                                </optgroup>
                        
                                <optgroup label="Lightspeed">
                                  <option value="lightSpeedIn">lightSpeedIn</option>
                                  <option value="lightSpeedOut">lightSpeedOut</option>
                                </optgroup>
                        
                                <optgroup label="Rotating Entrances">
                                  <option value="rotateIn">rotateIn</option>
                                  <option value="rotateInDownLeft">rotateInDownLeft</option>
                                  <option value="rotateInDownRight">rotateInDownRight</option>
                                  <option value="rotateInUpLeft">rotateInUpLeft</option>
                                  <option value="rotateInUpRight">rotateInUpRight</option>
                                </optgroup>
                        
                                <optgroup label="Rotating Exits">
                                  <option value="rotateOut">rotateOut</option>
                                  <option value="rotateOutDownLeft">rotateOutDownLeft</option>
                                  <option value="rotateOutDownRight">rotateOutDownRight</option>
                                  <option value="rotateOutUpLeft">rotateOutUpLeft</option>
                                  <option value="rotateOutUpRight">rotateOutUpRight</option>
                                </optgroup>
                        
                                <optgroup label="Specials">
                                  <option value="hinge">hinge</option>
                                  <option value="rollIn">rollIn</option>
                                  <option value="rollOut">rollOut</option>
                                </optgroup>
                        
                                <optgroup label="Zoom Entrances">
                                  <option value="zoomIn">zoomIn</option>
                                  <option value="zoomInDown">zoomInDown</option>
                                  <option value="zoomInLeft">zoomInLeft</option>
                                  <option value="zoomInRight">zoomInRight</option>
                                  <option value="zoomInUp">zoomInUp</option>
                                </optgroup>
                        
                                <optgroup label="Zoom Exits">
                                  <option value="zoomOut">zoomOut</option>
                                  <option value="zoomOutDown">zoomOutDown</option>
                                  <option value="zoomOutLeft">zoomOutLeft</option>
                                  <option value="zoomOutRight">zoomOutRight</option>
                                  <option value="zoomOutUp">zoomOutUp</option>
                                </optgroup>
                              </select>
                        
                              <button class="buttonme js--triggerAnimation">Animate it</button>
                            </form>
    					</div>
                        
                        <div class="col-md-4">
                        </div>
                    </div><!-- end clearfix -->
                </div><!-- end content -->
			</div><!-- end module --> 
		</div><!-- end container -->
    </section><!-- end white-wrapper-bg -->
	
<script>
  function testAnim(x) {
	  var $ = jQuery;
    $('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass();
    });
  };

  jQuery(document).ready(function($){
    $('.js--triggerAnimation').click(function(e){
      e.preventDefault();
      var anim = $('.js--animations').val();
      testAnim(anim);
    });

    $('.js--animations').change(function(){
      var anim = $(this).val();
      testAnim(anim);
    });
  });

</script>
	
<?php return ob_get_clean(); 