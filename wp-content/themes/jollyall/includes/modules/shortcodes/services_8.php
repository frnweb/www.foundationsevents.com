<?php wp_enqueue_script( array( 'owl.carousel.min' ) );
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat )  $query_args['services_category'] = $cat;

$query = new WP_Query($query_args) ; 
ob_start();?>


	<div class="white-wrapper clearfix">
		<div class="container">
			<div class="module">
				
				<?php if( $title ): ?>
				<div class="title wow zoomIn text-center clearfix">
					<h2><?php echo $title; ?></h2>
					<hr>
				</div><!-- end title -->
				<?php endif; ?>
				
				<div id="services_carousel">
                	
					<?php while( $query->have_posts() ): $query->the_post() ; 
						
						$meta = _WSH()->get_meta(); ?>
					
						<div itemscope itemtype="https://schema.org/Service" class="item">
							<div class="service-box">
								<div class="icon-container">
									<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
								</div><!-- icon-container -->
								<div class="title">
									<?php if( $link = sh_set( $meta, 'link' ) ): ?>
										<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<?php else: ?>
										<h3 itemprop="name"><?php the_title(); ?></h3>
									<?php endif; ?>
								</div><!-- end title -->
								<div class="desc">
									<p itemprop="text"><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
								</div><!-- end desc -->
							</div><!-- end service-box -->
						</div><!-- end col -->
					
					<?php endwhile; ?>
            
                </div><!-- end row -->
			</div><!-- end content --> 
		</div><!-- end container -->
    </div><!-- end white-wrapper -->


<?php wp_reset_query();
return ob_get_clean();

