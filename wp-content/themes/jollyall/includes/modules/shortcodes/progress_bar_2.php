<?php ob_start(); ?>

				<div class="halfscreen parallax" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
					<div class="container">
						<div class="module">

							<div class="module_widget">
                                
								<?php if( $title ): ?>
									<div class="title wow zoomIn text-center clearfix">
										<h2><?php echo $title; ?></h2>
										
										<hr>
                    					<p class="lead new-lead"><?php echo $text; ?></p>
									</div><!-- end title -->
									
								<?php endif; ?>
								
								
                                <?php if( $contents ): 
									
									$exp_contents = explode( "\n", $contents );
									
									if( $exp_contents ): ?>
									
										<div class="row">
											<div class="custom_stats">
												<div class="stat clearfix">
												
													<?php $chunks = array_chunk( $exp_contents, round( count( $exp_contents ) / 2 ) ); 
													
													if( $chunks ) foreach( $chunks as $chunk ):?>
													
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="about_skills">
																<?php if( $chunk ) foreach( $chunk as $ch ): 
																	
																	$values = explode( '|', $ch ); ?>
																	
																	<h4><?php echo sh_set($values, 0); ?> <span class="percent"><?php echo (int)sh_set($values, 1); ?>%</span></h4>
																	<div class="progress">
																		<div class="progress-bar wow fadeInLeftBig" role="progressbar" aria-valuenow="<?php echo (int)sh_set($values, 1); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo (int)sh_set($values, 1); ?>%"></div> 
																	</div>
																
																<?php endforeach; ?>
															</div>
														</div>
													
													<?php endforeach; ?>
												</div>
											</div>
										</div>
									
									<?php endif; ?>
									 
								<?php endif; ?>
							</div><!-- end module_widget -->
						</div>
					</div>
				</div>

<?php return ob_get_clean();


