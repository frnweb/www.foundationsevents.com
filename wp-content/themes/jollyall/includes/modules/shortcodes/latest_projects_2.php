<?php 
$query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
//if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $cat));
if( $cat ) $query_args['projects_category'] = $cat;
$query = new WP_Query($query_args) ;  
//wp_enqueue_script( array('jquery.isotope.min')); 
ob_start();

?>

<div class="grey-wrapper clearfix">
	<div class="container">
		<div class="module text-center">

			<?php if( $title ): ?>
				<div class="title wow zoomIn clearfix animated">
					<h2><?php echo $title; ?></h2>
					<hr>
				</div><!-- end parallax-title -->
			<?php endif; ?>
			
			<div class="row" id="latest-items">
				
				<?php while( $query->have_posts() ): $query->the_post(); ?>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div itemscope itemtype="http://schema.org/CreativeWork" class="portfolio-item entry">
						
						<?php the_post_thumbnail('501x331', array('class'=>'img-responsive', 'itemprop'=>'image' ) ); ?>

						<div class="magnifier">
							<div class="buttons">
								<h3>
								<a itemprop="name" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h3>
								<hr>
								<p><?php the_terms(get_the_id(), 'projects_category', '', ', ' ); ?></p>
							</div><!-- end buttons -->
						</div><!-- end magnifier -->
					</div><!-- end portfolio-item -->
				</div><!-- end col -->
				
				<?php endwhile; ?>
				
				<?php if( $link ): ?>
				<div class="col-md-12 button-wrapper clearfix text-center">
					<a class="btn btn-primary btn-dark" title="<?php _e('View All Works', SH_NAME); ?>" href="<?php echo esc_url($link); ?>"><?php _e('View All Works', SH_NAME); ?></a>		
				</div><!-- end button-wrapper -->  
				<?php endif; ?>
			</div><!-- end row -->
		</div><!-- end module --> 
	</div><!-- end container -->
</div>

<?php wp_reset_query();
return ob_get_clean(); ?>
