<?php 
$query_args = array('post_type' => 'sh_faq' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order );
if( $cat ) $query_args['faq_category'] = $cat;
$query = new WP_Query($query_args) ; 

ob_start(); 
?>

	<div class="white-wrapper clearfix">
		<div class="no-container">
			<div class="content-module">
           		<div class="content fullwidth">
					
						<?php if( $title ): ?>
							<div class="title">
								<h3><?php echo $title; ?></h3>
							</div><!-- end title -->
						<?php endif; ?>
						
						<?php if( $contents ): ?>
							<?php echo $contents; ?>
							<hr>
						<?php endif; ?>


                        <div id="accordion-widget" class="panel-group">
                            
							<?php while( $query->have_posts() ): $query->the_post(); ?>
							
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php the_ID(); ?>"><?php the_title(); ?></a>
										</h4>
									</div>
									<div id="collapseOne<?php the_ID(); ?>" class="panel-collapse collapse<?php if( $query->current_post == 0 ) echo ' in'; ?>">
										<div class="panel-body">
											<?php the_content(); ?>
										</div>
									</div>
								</div>
                            
							<?php endwhile; ?>
							
							<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Clean & Professional Design Really?</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">How Much Valid HTML5 Page Templates?</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                                    </div>
                                </div>
                            </div>   
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Detailed Documentation Included to the .zip?</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How can I get 24/7 Customer Support?</a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end panel-group -->
                                             
                </div><!-- end content -->
			</div><!-- end module --> 
		</div><!-- end container -->
    </div><!-- end white-wrapper-bg -->


<!-- end grey-wrapper -->

<?php wp_reset_query();
return ob_get_clean();


