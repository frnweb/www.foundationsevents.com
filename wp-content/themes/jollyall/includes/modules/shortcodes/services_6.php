<?php
$count = 1;
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 

ob_start();?>




	<div class="module_widget">
		
		<?php if( $title ): ?>
		
		<div class="title">
			<h3><?php echo $title; ?></h3>
		</div><!-- end title -->
		
		<?php endif; ?>

		
		<div id="accordion-widget" class="panel-group">
			
			<?php while( $query->have_posts() ): $query->the_post(); ?>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h4>
				</div>
				<div id="collapseOne<?php the_ID(); ?>" class="panel-collapse collapse<?php if( $query->current_post == 0 ) echo ' in'; ?>">
					<div class="panel-body">
						<?php echo _sh_trim( get_the_content(), $limit ); ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>

		</div><!-- end panel-group -->

	</div><!-- end module widget -->


<?php wp_reset_query();
return ob_get_clean();





