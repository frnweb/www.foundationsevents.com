<?php $query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['projects_category'] = $cat;
$query = new WP_Query($query_args) ; 

ob_start();?>

	<div class="grey-wrapper clearfix">
        <div class="container">
            <div class="module text-center">
                
				<?php if( $title ): ?>
					<div class="title wow zoomIn text-center clearfix">
						<h2><?php echo $title; ?></h2>
						
						<?php if( $tagline ): ?>
							<hr>
							<p class="lead new-lead"><?php echo $tagline; ?></p>
						<?php endif; ?>
					</div><!-- end title -->
				<?php endif; ?>
                
                <div id="latest-items" class="row">
                    
					<?php while( $query->have_posts() ): $query->the_post(); ?>
					
						<div itemscope itemtype="http://schema.org/CreativeWork" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="custom-portfolio-item">
							
								<?php if( has_post_thumbnail() ): ?>
								
									<a data-gal="prettyPhoto[product-gallery]" rel="bookmark" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" title="<?php the_title_attribute(); ?>">		
										<figure class="effect-bubba">
											
											<?php the_post_thumbnail( '501x331', array( 'class'=>'img-responsive', 'itemprop' => 'image' ) ); ?>
											
	
											<figcaption>
												<h2><i class="fa fa-search"></i></h2>
												<?php $term_list = wp_get_post_terms(get_the_id(), 'projects_category', array("fields" => "names"));?>
												<p><?php if( $term_list ) echo implode( ', ', $term_list ); ?></p>
											</figcaption>           
										</figure>
									</a>
								
								<?php endif; ?>
								
							   <div class="title">
									<h4 itemprop="name"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
								</div><!-- end title -->
							</div>
						</div><!-- end col -->
					
					<?php endwhile; ?>
					
                    <?php if( $link ): ?>
						<div class="col-md-12 button-wrapper clearfix text-center">
							<a href="<?php echo esc_url( $link ); ?>" title="<?php echo $title; ?>" class="btn btn-primary btn-dark"><i class="fa fa-search"></i> <?php _e('View All Portfolios', SH_NAME); ?></a>
						</div><!-- end button-wrapper -->     
					<?php endif; ?>
                </div><!-- end row -->
            </div><!-- end module --> 
        </div><!-- end container -->
    </div><!-- end transparent-bg -->


<?php wp_reset_query();
return ob_get_clean(); ?>

