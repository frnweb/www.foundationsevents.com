

<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				
				<div class="modalcart">
                    <div class="modalcart_title">
					
						<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() )
								echo $thumbnail;
							else
								printf( '<a href="%s">%s</a>', $_product->get_permalink(), $thumbnail );
						?>
						<h2>
							<?php
							if ( ! $_product->is_visible() )
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
							else
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', $_product->get_permalink(), $_product->get_title() ), $cart_item, $cart_item_key );?>		
						</h2>

                        <div class="modalcart_meta_pricing">
                            <p>
								<i class="fa fa-dollar"></i> 
								<?php
									echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
								?>
							</p>
                        </div><!-- end modalcart_meta_pricing -->
                        <div class="modalcart_meta_cat">
                            <p><a href="#" class="comments-link" title="Shop Item"><i class="fa fa-shopping-cart"></i> Woman Dresses</a></p>
                        </div><!-- end modalcart_meta_cat -->
                    </div><!-- end modalcart_title -->
                </div>
				<hr />
		<?php	}
		}
return ;?>
