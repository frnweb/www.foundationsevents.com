<?php $theme_options = _WSH()->option();

if( !sh_set( $theme_options, 'ajax_loader' ) ) return;

//useful variables
$uploads = wp_upload_dir();
$GLOBALS['AAPLimages'] = $uploads['basedir'] . '/AAPL';
$GLOBALS['AAPLimagesurl'] = $uploads['baseurl'] . '/AAPL';


// Set Hook for outputting JavaScript
add_action('wp_head', '_sh_header_custom_data_');
add_action('wp_footer', '_sh_footer_custom_data_');
add_action('wp_enqueue_scripts', '_sh_ajax_enqueue_');

function _sh_ajax_enqueue_() {
	
	wp_register_script('sh_ajax-page-loader', get_template_directory_uri(). '/includes/thirdparty/ajax-loader/refresh-page.js', '', '', true);
	wp_register_script('sh_reload-code', get_template_directory_uri(). '/includes/thirdparty/ajax-loader/refresh.js', '', '', true);
	wp_register_script('sh_animation_img', get_template_directory_uri(). '/includes/thirdparty/ajax-loader/animation_img.js', '', '', true);
    wp_enqueue_script(array('jquery', 'sh_ajax-page-loader', 'sh_reload-code', 'sh_animation_img'));
}

function _sh_footer_custom_data_() {
	
}

function _sh_header_custom_data_() {
	
	$theme_options = _WSH()->option();
	//This goes into the header of the site.
	
	$loading_code = '<center>
						<p style="text-align: center !important;">Loading... Please Wait...</p>
						<p style="text-align: center !important;">
							<img src="{loader}" border="0" alt="Loading Image" title="Please Wait..." />
						</p>
					</center>';
	$loadin_error_layout = '<center>
								<p style="text-align: center !important;">Error!</p>
								<p style="text-align: center !important;">
									<font color="red">There was a problem and the page didnt load.</font>
								</p>
							</center>';
	
	?>
	
	<script type="text/javascript">
	
		var SHhome = "<?php echo home_url();?>";
	
		<?php $transitent = get_transient( '_sh_ajax_loader_update' );
		
		if( $transitent === false ) {

			ob_start(); ?>
			
			//reload code here
			var AAPLloadingIMG = jQuery('<img/>').attr('src', '<?php echo get_template_directory_uri() . '/includes/thirdparty/ajax-loader/loaders/' . sh_set( $theme_options, 'ajax_loader_images' ) ;?>');
			var AAPLloadingDIV = jQuery('<div/>').attr('style', 'display:none;').attr('id', 'ajaxLoadDivElement');
			AAPLloadingDIV.appendTo('body');
			AAPLloadingIMG.appendTo('#ajaxLoadDivElement');
			
			
			var str = <?php echo json_encode($loading_code); ?>;
			var AAPL_loading_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));
			str = <?php echo json_encode($loadin_error_layout); ?>;
			var AAPL_loading_error_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));
			<?php $new_content = ob_get_clean(); 
			file_put_contents( get_template_directory().'/includes/thirdparty/ajax-loader/animation_img.js', $new_content );
			set_transient( '_sh_ajax_loader_update', true, DAY_IN_SECONDS );
		
		}?>
	</script>
	<?php 
}



