<?php /* Template Name: Landing Page */?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title><?php wp_title(''); ?></title>

<!-- Favicons - Touch Icons -->
<link rel="icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri();?>/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri();?>/images/apple-touch-icon-114x114.png">

<!-- Responsive Styles -->
<link rel="stylesheet" type="text/css" href="http://jollythemes.com/html/jollyall/css/responsive.css" />
<!-- Bootstrap Styles -->
<link type="text/css" rel="stylesheet" href="http://jollythemes.com/html/jollyall/css/bootstrap.css" media="screen">
<!-- Carousel -->
<link href="http://jollythemes.com/html/jollyall/css/owl.carousel.css" rel="stylesheet" type="text/css">
<!-- FlexSlider -->
<link href="http://jollythemes.com/html/jollyall/css/flexslider.css" rel="stylesheet" type="text/css">
<!-- Animate Styles -->
<link href="http://jollythemes.com/html/jollyall/css/animate.min.css" rel="stylesheet" type="text/css">
<!-- Lightbox Styles -->
<link href="http://jollythemes.com/html/jollyall/css/prettyPhoto.css" rel="stylesheet" type="text/css">
<!-- General Styles -->
<link href="http://jollythemes.com/html/jollyall/style.css" rel="stylesheet" type="text/css">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="http://jollythemes.com/html/jollyall/css/extralayers.css" media="screen" />	
<link rel="stylesheet" type="text/css" href="http://jollythemes.com/html/jollyall/rs-plugin/css/settings.css" media="screen" />

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300,600,700,800' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri();?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

    <!-- Switcher-->
    <link type="text/css" rel="stylesheet" id="switcher-css" href="switcher/css/switcher.css" media="all">
    
    <!-- Demo Examples (For Module #3) -->
        <link rel="alternate stylesheet" type="text/css" href="switcher/css/dark.css" title="dark" media="all" />
    <!-- END Demo Examples -->
    
</head>
<body id="landing-page">


	<div class="animationload">
    <div class="loader">Loading...</div>
	</div> 

    <div id="header-container">
        <header class="header container transparent">
            <div class="menu-wrapper">
                <nav id="navigation" class="navbar yamm" role="navigation">
                    <div class="navbar-inner">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-bars fa-2x"></i>
                            </button>
						<a id="brand" class="pulse navbar-brand" data-scroll data-options='{ "easing": "easeInQuad" }' href="<?php echo home_url(); ?>"><img src="http://jollythemes.com/html/jollyall/images/logo.png" alt="Jollyall"></a>
                        </div><!-- end navbar-header -->
                        <div id="navbar-collapse-1" class="navbar-right navbar-collapse collapse">
                            <ul class="nav navbar-nav  scrollable-menu">
                                <li class="hover-it"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#why">Why Choosing Jollyall</a></li>
                                <li class="hover-it"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#features">Features</a></li>
                                <li class="hover-it"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#demos">Demos</a></li>
                                <li class="hover-it"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#video-doc">Video Documentation</a></li>
                                <li><a href="#" class="btn btn-primary btn-sm">Buy Now</a></li>
                            </ul><!-- end navbar-right -->
                        </div><!-- end navbar-callopse -->
                    </div><!-- end navbar-inner -->
                </nav><!-- end navigation -->
            </div><!-- menu wrapper -->
        </header><!-- end header -->
    </div><!-- end header container -->
    
	<div id="slideshow" class="tp-banner-container">
		<div class="tp-banner">
        	<ul>
			<li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg-mini.jpg" data-delay="10000" data-saveperformance="off" data-title="Next Slide">
                <!-- MAIN IMAGE -->
                <img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png"  
                alt="" 
                data-duration="12000" 
                data-ease="Power0.easeInOut" 
                data-bgfit="115"
                data-lazyload="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg.jpg" 
                data-bgrepeat="no-repeat" 
                data-bgposition="left center"
                data-kenburns="on"
                data-bgfitend="130"
                data-bgpositionend="center top">
                <!-- LAYERS -->

                <!-- LAYER NR. 2 -->
            <div class="tp-caption lft customout rs-parallaxlevel-0"
                data-x="center"
                data-y="66" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1400"
                data-start="1000"
                data-easing="Power3.easeInOut"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 3;"><img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png" alt="" data-lazyload="http://jollythemes.com/html/jollyall/images/demos/slider_logo.png">
            </div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_601 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="300" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1200"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">Jollyall for all purpose
			</div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_602 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="430" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1500"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">We Designed with your future in mind - <strong>Jollythemes</strong>
			</div>

			<!-- LAYER NR. 10 -->
			<div class="tp-caption btn-slider skewfromleft randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="540" 
                data-speed="1000"
                data-start="3800"
                data-easing="Power3.easeInOut"
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9400"
                data-endspeed="1000"
                style="z-index: 111; max-width: auto; max-height: auto; white-space: nowrap;"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#demos" class="btn btn-primary btn-transparent">Select Demo</a>
			</div> 
                
			</li>
            
			<li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg-mini.jpg" data-delay="10000" data-saveperformance="off" data-title="Next Slide">
                <!-- MAIN IMAGE -->
                <img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png"  
                alt="" 
                data-duration="12000" 
                data-ease="Power0.easeInOut" 
                data-bgfit="115"
                data-lazyload="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg.jpg" 
                data-bgrepeat="no-repeat" 
                data-bgposition="left center"
                data-kenburns="on"
                data-bgfitend="130"
                data-bgpositionend="center top">
                <!-- LAYERS -->

                <!-- LAYER NR. 2 -->
            <div class="tp-caption lft customout rs-parallaxlevel-0"
                data-x="center"
                data-y="66" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1400"
                data-start="1000"
                data-easing="Power3.easeInOut"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 3;"><img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png" alt="" data-lazyload="http://jollythemes.com/html/jollyall/images/demos/slider_logo.png">
            </div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_601 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="300" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1200"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">Jollyall, Next generation WordPress theme
			</div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_602 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="430" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1500"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">We Designed with your future in mind - <strong>Jollythemes</strong>
			</div>

			<!-- LAYER NR. 10 -->
			<div class="tp-caption btn-slider skewfromleft randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="540" 
                data-speed="1000"
                data-start="3800"
                data-easing="Power3.easeInOut"
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9400"
                data-endspeed="1000"
                style="z-index: 111; max-width: auto; max-height: auto; white-space: nowrap;"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#demos" class="btn btn-primary btn-transparent">Select Demo</a>
			</div> 
                
			</li>

			<li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg-mini.jpg" data-delay="10000" data-saveperformance="off" data-title="Next Slide">
                <!-- MAIN IMAGE -->
                <img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png"  
                alt="" 
                data-duration="12000" 
                data-ease="Power0.easeInOut" 
                data-bgfit="115"
                data-lazyload="http://jollythemes.com/html/jollyall/images/demos/landing-page-bg.jpg" 
                data-bgrepeat="no-repeat" 
                data-bgposition="left center"
                data-kenburns="on"
                data-bgfitend="130"
                data-bgpositionend="center top">
                <!-- LAYERS -->

                <!-- LAYER NR. 2 -->
            <div class="tp-caption lft customout rs-parallaxlevel-0"
                data-x="center"
                data-y="66" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1400"
                data-start="1000"
                data-easing="Power3.easeInOut"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 3;"><img src="http://jollythemes.com/html/jollyall/images/demos/dummy.png" alt="" data-lazyload="http://jollythemes.com/html/jollyall/images/demos/slider_logo.png">
            </div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_601 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="300" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1200"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">Trendy Design and Endless possibilities
			</div>

			<!-- LAYER NR. 9 -->
			<div class="tp-caption black_heavy_602 customin randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="430" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1500"
                data-easing="Power4.easeOut"
                data-splitin="chars"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9200"
                data-endspeed="1000"
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">We Designed with your future in mind - <strong>Jollythemes</strong>
			</div>

			<!-- LAYER NR. 10 -->
			<div class="tp-caption btn-slider skewfromleft randomrotateout tp-resizeme rs-parallaxlevel-10"
                data-x="center"
                data-y="540" 
                data-speed="1000"
                data-start="3800"
                data-easing="Power3.easeInOut"
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-end="9400"
                data-endspeed="1000"
                style="z-index: 111; max-width: auto; max-height: auto; white-space: nowrap;"><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#demos" class="btn btn-primary btn-transparent">Select Demo</a>
			</div> 
                
			</li>


			</ul>
		</div><!-- tp-banner -->
	</div><!-- end slider -->
    
	<section id="why" class="white-wrapper">
		<div class="container">
			<div class="module clearfix">
				<div class="title text-center clearfix">
					<h2>WHY CHOOSING JOLLYALL?</h2>
					<hr>
                    <p class="lead">Jollyall, The Smartest Way To Grow Your Business Next Level, The one and final WordPress theme you'll ever have to buy.</p>
				</div><!-- end title -->
                
                <div class="module clearfix aligncenter text-center">
                <img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_01.png" class="wow fadeInUp" data-wow-duration="3s" alt="">
                </div>
            
				<div class="row">
                	<div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="service-box">
                        	<div class="icon-container with-border wow rotateInDownLeft">
                            	<i class="fa fa-gears"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>Perfect Responsive</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>Jollyall is a beautiful responsive resize to fit the size or your browser or screen! It fully responsive and perfectly fits on any device.</p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
                    </div><!-- end col -->
                	<div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="service-box">
                        	<div class="icon-container with-border wow rotateInDownLeft">
                            	<i class="fa fa-bullhorn"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>Powerful Admin Panel</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>This panel will cover all your basic needs, so you don't need to touch a single line of code, perfect for people without any coding knowledge.</p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
                    </div><!-- end col -->
                	<div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="service-box">
                        	<div class="icon-container with-border wow rotateInDownLeft">
                            	<i class="fa fa-camera-retro"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>Free Support</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>We consider support as important as our theme development. If you need help, get all of your questions answered quickly with exclusive.</p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
                    </div><!-- end col -->
                  
                </div><!-- end row -->
			</div><!-- end content --> <br><br>
		</div><!-- end container -->
    </section><!-- end white-wrapper -->

	<section class="grey-wrapper">
		<div class="container">
			<div class="module clearfix">
				<div class="title text-center clearfix">
					<h2>JOLLYALL UNBELIVABLE FEATURES</h2>
					<hr>
                    <p class="lead">Jollyall Designed For All-Purpose, The one and final WordPress theme you'll ever have to buy.</p>
				</div><!-- end title -->
                
				<div class="row module clearfix">
                	<div class="col-md-7 col-sm-7 col-xs-12">
                    	<img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_02.png" class="wow fadeInUp img-responsive" data-wow-duration="2s" alt="">
                    </div><!-- end col -->
                	<div class="col-md-5 col-sm-5 col-xs-12">
						<div class="title">
                        	<h4>Responsive, Retina Ready & Clean</h4>
                        </div>
                        
                        <div class="desc">
							<p>Praesent gravida metus hendrerit consequat gravida. Maecenas posuere vitae nisl ornare venenatis. Aenean id laoreet magna, ac hendrerit dolor.</p>
						</div><!-- end desc -->
                    
						<div class="service-list clearfix">
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-laptop"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Perfect Responsive Design</h3>
                                </div><!-- end title -->
                                <div class="desc">
                                    <p>Jollyall is fully responsive and perfectly fits on any device..</p>
								</div><!-- end desc -->
							</div><!-- end service-box -->
                            
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-cubes"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Retina Ready</h3>
                                </div><!-- end title -->
                                <div class="desc">
                                    <p>Jollyall automatically creates retina ready images.</p>
								</div><!-- end desc -->
							</div><!-- end service-box -->
						</div><!-- end list -->
                    </div><!-- end col -->              
				</div><!-- end row -->
			</div><!-- end content --> 
		</div><!-- end container -->
    </section><!-- end white-wrapper -->

	<section class="white-wrapper clearfix">
		<div class="container">
			<div class="module">
            	<div class="col-lg-5 col-md-5 col-xs-12">
     				<div class="title">
                    	<h4>Superb Dark & Light Versions</h4>
                    </div>
                    <div class="desc">
                    	<p>Phasellus semper massa sit amet metus dictum bibendum. Donec at dui ipsum. Maecenas sagittis tincidunt magna. Proin vulputate mauris porttitor semper venenatis. Nulla quis sem eget dolor eleifend elementum id at lorem.</p>

						<p>Nunc faucibus venenatis aliquet. Proin interdum nunc sit amet nisl auctor, eu suscipit tortor commodo. Mauris in ultricies.</p>
                        <br>
                        <a data-scroll data-options='{ "easing": "easeInQuad" }' href="#demos" title="" class="btn btn-primary">Quick Demo</a>
                    </div><!-- end desc -->
                </div><!-- end col -->
                
            	<div class="col-lg-7 col-md-7 col-xs-12">
					<img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_03.png" class="wow rotateInDownRight img-responsive" data-wow-duration="2s" alt="">
                </div><!-- end col -->
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->

    <section class="grey-wrapper clearfix">
		<div class="container">
			<div class="module clearfix">
            	<div class="col-lg-6 col-md-6 col-xs-12"><br><br>
					<img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_04.png" class="wow rotateInDownLeft img-responsive" data-wow-duration="2s" alt="">
                </div><!-- end col -->
            	<div class="col-lg-6 col-md-6 col-xs-12">
     				<div class="title">
                    	<h4>WooCommerce Ready</h4>
                    </div>
                    <div class="desc">
                    	<p>WooCommerce is the most popular WP eCommerce plugin. And it's available for free. Packed full of features, perfectly integrated into your self-hosted WordPress website. Jollyall 100% compatible with WooCommerce!</p>
					</div><!-- end desc -->
                    <div class="service-list list-3">
						<div class=" clearfix">
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>6 Beautiful Awesome Shopping Pages</h3>
                                </div><!-- end title -->
							</div><!-- end service-box -->
                            
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Payments, Shipping, Inventory, Reporting, Marketing, Tax!</h3>
                                </div><!-- end title -->
							</div><!-- end service-box -->
                            
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Tons of features and easy to use and customiz</h3><br>
                                </div><!-- end title -->
							</div><!-- end service-box -->
						</div><!-- end list -->
					</div><!-- end row -->
                </div><!-- end col -->
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->
    
	<section class="white-wrapper clearfix">
		<div class="container">
			<div class="module">
            	<div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="service-list list-2 clearfix">
                        <div class="title wow zoomIn">
                        <br>
                            <h4>Jollyall, The Smartest Way To Grow Your Business Next Level</h4>
                        </div><!-- end title -->
						<div class="service-box clearfix">
                        	<div class="icon-container wow rollIn">
                            	<i class="fa fa-cubes"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>Unlimited Variations of Home pages</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>We have a  content, design and development, our in-house producion team tightly collaborates to carry out large-scaled projects.</p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
                        
						<div class="service-box">
                        	<div class="icon-container wow rollIn">
                            	<i class="fa fa-database"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>Simple, flexible & loaded with LOTS OF features.</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>People ask us why we get so excited about Jollyall themes. It&acute;s not about WordPress themes, it&acute;s about helping people build better websites for them or their clients.</p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
                        
						<div class="service-box">
                        	<div class="icon-container wow rollIn">
                            	<i class="fa fa-empire"></i>
                            </div><!-- icon-container -->
                            <div class="title">
                            	<h3>PURCHASE JOLLYALL & GET 5 Star SUPPORT</h3>
                            </div><!-- end title -->
                            <div class="desc">
                            	<p>Aliquam rhoncus molestie auctor. Suspendisse ac risus eros. Vivamus faucibus auctor nibh id semper. </p>
                            </div><!-- end desc -->
                        </div><!-- end service-box -->
					</div><!-- end service-list -->
                </div><!-- end col -->
                
            	<div class="col-lg-6 col-md-6 col-xs-12">
                	<img src="http://jollythemes.com/html/jollyall/images/demos/iphone_07.png" class="wow rotateInDownRight img-responsive" alt="">
                </div><!-- end col -->
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->

    <section class="grey-wrapper clearfix">
		<div class="container">
			<div class="module clearfix">
            	<div class="col-lg-6 col-md-6 col-xs-12">
					<img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_05.png" class="wow rotateInDownLeft img-responsive" data-wow-duration="2s" alt="">
                </div><!-- end col -->
            	<div class="col-lg-6 col-md-6 col-xs-12"><br><br>
     				<div class="title">
                    	<h4>Powerful Theme Options Easy to use</h4>
                    </div>
                    <div class="desc">
                    	<p>The Jollyall WordPress theme comes with super awesome theme options panel! Is this panel build-in Vafpress Framework with tons of options!</p>
					</div><!-- end desc -->
                    <div class="service-list list-3">
						<div class=" clearfix">
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Font, color, background changer scripts.</h3>
                                </div><!-- end title -->
							</div><!-- end service-box -->
                            
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>SEO settings, social media management systems...</h3>
                                </div><!-- end title -->
							</div><!-- end service-box -->
                            
                            <div class="service-box">
                                <div class="icon-container wow rollIn">
                                    <i class="fa fa-mail-forward"></i>
                                </div><!-- icon-container -->
                                <div class="title">
                                    <h3>Header, footer and much more options!</h3><br>
                                </div><!-- end title -->
							</div><!-- end service-box -->
						</div><!-- end list -->

					</div><!-- end row -->
                </div><!-- end col -->
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->

	<section class="halfscreen parallax" style="background-image: url('http://jollythemes.com/html/jollyall/images/demos/parallax_09.png');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay dark-version nopadding">
        	<div class="container">
            	<div class="client_module">
                    <div class="title">
                            <h2>Jollyall contains Awesome <span id="rotateme">IMAGE</span> parallax backgrounds</h2>
                    </div><!-- end parallax-title -->
                    <div class="text-center banner-img">
                    	<img class="img-responsive" src="http://jollythemes.com/html/jollyall/images/demos/iphone_08.png" alt="">
                    </div><!-- text-center -->
				</div><!-- end module --> 
            </div><!-- end container -->
    	</div><!-- end overlay -->
    </section><!-- end transparent-bg -->

	<section id="features" class="white-wrapper">
		<div class="container">
			<div class="module clearfix">
            
            	<div class="row text-center">
                	<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_01.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Mega Menu Plugin ($12)</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>You can place any content in dropdowns: links, text, images, widgets and shortcodes. Custom dropdowns so easy with &ldquo;Mega Main Menu&rdquo;.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_02.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Revolution Slider ($16)</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Create a responsive (mobile friendly) or fullwidth slider with must-see-effects and meanwhile keep or build your SEO optimization.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                    
					<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_03.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Visual Page Builder ($25)</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Visual Composer for WordPress is drag and drop frontend and backend page builder plugin that will save you tons of time working on the site content, so you don't need to touch a single line of code, perfect for people without any coding. knowledge.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_04.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>WooCommerce Compatible</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>WooCommerce is the most popular WordPress eCommerce plugin. And it's available for free. Packed full of features, perfectly integrated into your self-hosted WordPress website.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                    
               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_05.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Powerful Theme Options Panel</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>The Jollyall WordPress theme comes with super awesome theme options panel! Jollyall WP theme panel build-in Vafpress Framework with tons of options!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_06.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>WPML - Multi Language Ready</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>This theme 100% support for WPML plugin! You can use this theme on your language! We wil add more options for multi-language for you! </p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_07.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Custom Post Formats</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Add standard, audio, video, quote and gallery posts using unique custom post formats. The Jollyall WPs theme comes with standard and unique post formats!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_08.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>70+ Widgets and Modules</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>We build 70+ premium and creative widgets and modules for you. All widgets and modules compatible with Visual Composer page builder plugin!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_09.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>2 Different Header Styles</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>The Jollyall WordPress theme comes with 2 different header and menu styles. Ex: centered logo menu with top bar, simple menu with top bar.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_10.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Search Engine Optimize</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>The Jollyall WordPress theme compatible with all search engine optimization recommendeds. Also we build awesome SEO options for all your needs.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_11.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Unlimited Color Schemes</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Did you check our theme options panel for color schemes? We added an amazing color changer script for you! You can build your own color schemes just a minutes!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_12.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Detailed Documentation</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>If you are impressed with Jollyall, then you'll be blown away by the exceptional after sale support! We offer support forum, theme documentation and 24/7 support via email!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_13.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Google Fonts Included</h4>
                            </div><!-- end title -->
							

                            <div class="desc">
                            	<p>Change the look and feel of your website with include Google Font options. Use the theme option panel to change the fonts for the default body, menu, heading</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_14.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Font Awesome Icons</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Get creative with Font Awesome icon support! The Jollyall WP theme give you the power to add icons using over +350 infinitely scalable icons available.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_15.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Layered PSD Files Included</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>12$ values of Layered PSD files are included for all page layouts and graphic buttons. These files are good organized in folders to easy to change color palettes or content.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_16.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Custom Blog Templates</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Jollyall theme comes with creative blog template layouts. You can build your own blog styles with powerful page builder module! Like masonry, 3-4 columns, magazine style and much more.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                    
               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_17.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Custom Portfolio Templates</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>This theme comes with creative portfolio page examples. We build pagination and filterable style portfolio pages. And in our portfolio section we added masonry, classic and gallery styles!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_18.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Custom Shop Templates</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Jollyall WordPress theme coded with custom shop page templates (WooCommerce). If you are looking professional e-Commerce theme for your shop projects, you are right place!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                    
               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_19.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Powerful Shortcode Elements</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Did you check our shortcode elements? The Jollyall comes with awesome shortcode elements like tabs, accordions, buttons, pricing tables and much more.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_20.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>One-Click Installation</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>Jollyall WordPress theme comes with demo content XML file. Demo content let's you check all of available options for slider elements, theme options, site blog and page examples.</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->

               		<div class="col-md-6 first">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_21.png" alt="" class="wow bounceInLeft img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Maintenance Mode Page</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>You need to create a stunning and functional coming soon website? Dont worry! The Jollyall WordPress theme comes with 2 different style maintenance mode pages!</p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                
                	<div class="col-md-6 last">
                    	<div class="features">
                			<div class="media-object">
                            	<img src="http://jollythemes.com/html/jollyall/images/demos/features_22.png" alt="" class="wow bounceInRight img-circle img-thumbnail">
                            </div>
                            <div class="title">
                                <h4>Left & Right Menu Options</h4>
                            </div><!-- end title -->
							
                            <div class="desc">
                            	<p>- Left menu with optional background from page to page<br>
								   - Right menu with optional background from page to page<br>
									- Page transition (ajax) with fade effect.
                                </p>
                            </div><!-- end desc -->
                        </div><!-- end features -->
                	</div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->

	<section class="halfscreen parallax" style="background-image: url('http://jollythemes.com/html/jollyall/images/demos/parallax_07.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay dark-version">
        	<div class="container">
            	<div class="module">
					<div class="row">
                        <div class="stat clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="milestone-counter clearfix">
                                    <span class="stat-count highlight_mini">20+</span>
                                    <div class="milestone-details_mini">Unique Home Pages Examples</div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="milestone-counter clearfix">
                                    <span class="stat-count highlight_mini">10+</span>
                                    <div class="milestone-details_mini">Example Site Layouts</div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="milestone-counter clearfix">
                                    <span class="stat-count highlight_mini">900+</span>
                                    <div class="milestone-details_mini">Hours Worked</div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="milestone-counter clearfix">
                                    <span class="stat-count highlight_mini">100+</span>
                                    <div class="milestone-details_mini">HTML Page Templates</div>
                                </div>
                            </div>
                        </div><!-- stat -->
                    </div><!-- end row -->
                </div><!-- end module -->
            </div><!-- end container -->
    	</div><!-- end overlay -->
    </section><!-- end transparent-bg -->

	<section id="demos" class="white-wrapper">
		<div class="container">
				<div class="title text-center clearfix">
					<h2>TIME TO SEE THE JOLLALL DEMOS, DEMOS</h2>
					<hr>
                    <p class="lead">Let's see all available layouts; Creative, Portfolio, Shop, Blog, Dark Version, Headers, Footers, Classic</p>
				</div><!-- end title -->

				<div class="portfolio_wrapper clearfix">
                    <div class="title">
                        <nav class="portfolio-filter text-center clearfix">
                            <ul>
                                <li><a class="btn active btn-primary" href="#" data-filter="*"><span></span>All</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".creative">Creative</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".portfolio">Portfolio</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".blog">Blog</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".shop">Shop</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".boxed">Boxed</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".header-styles">Headers</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".footer-styles">Footers</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".landing-page">Landing Page</a></li>
                                <li><a class="btn btn-primary" href="#" data-filter=".maintenance">Maintenance</a></li>
                            </ul>
                        </nav>
                    </div>
   
           </div><!-- end container -->
				</div><!-- end portfolio_wrapper -->


                    <div class="masonry_wrapper clearfix">

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_22.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_24.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-1.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_23.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-2.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_25.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-3.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_26.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-4.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_27.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-5.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_28.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-6.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_29.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-7.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_30.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-8.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_31.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-9.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 creative">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_32.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-10.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 boxed">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_07.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="index-boxed.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 footer-styles">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_03.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="features-footer-multi-columns.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 footer-styles">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_04.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="features-footer-default.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 header-styles">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_05.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="features-header-logo-center.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 header-styles">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_06.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="features-header-default.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

            

                        <div class="item item-h2 maintenance">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_01.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="<?php echo home_url(); ?>" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_16.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-masonry-1.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_17.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-masonry-2.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_18.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-classic-1.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_19.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-classic-2.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->
                        
                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_20.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-gallery-1.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 portfolio">
                        	<div class="entry">
                            <a href="<?php echo home_url(); ?>" target="_blank"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_21.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="portfolio-gallery-2.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 landing-page">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_02.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" href="features-why-choose.html" target="_blank" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 blog">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_12.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="blog-style-1.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->
                        
                        <div class="item item-h2 blog">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_13.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="blog-style-2.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->
                        
                        <div class="item item-h2 blog">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_14.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="blog-style-3.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 blog">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_15.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="blog-style-4.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 shop">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_08.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="shop-fullwidth.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->
                        
                        <div class="item item-h2 shop">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_09.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="shop-style-1.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->
                        
                        <div class="item item-h2 shop">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_10.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="shop-style-2.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                        <div class="item item-h2 shop">
                        	<div class="entry">
                            <a href="#"><img src="http://jollythemes.com/html/jollyall/images/demos/preview/demo_11.jpg" alt=""></a>
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="btn btn-lg btn-primary" target="_blank" href="shop-style-3.html" title="">View Demo</a></h3>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            </div>
                        </div><!-- end item -->

                    </div><!-- end masonry_wrapper --><br><br>
                    
                    
    </section><!-- end white-wrapper -->

	<section class="halfscreen parallax" style="background-image: url('http://jollythemes.com/html/jollyall/images/demos/parallax_07.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay dark-version">
        	<div class="container">
            	<div class="module">
					<div class="welcome_message text-center clearfix">
                    	<h2>So you're Satisfied that Jollyall is the best?</h2>
                        <div class="button-wrapper">
                        	<a href="#" class="btn btn-primary btn-lg btn-transparent">Buy Now On Themeforest</a>
                        </div><!-- end button-wrapper -->
                    </div><!-- end welcome_message -->
                </div><!-- end module -->
            </div><!-- end container -->
    	</div><!-- end overlay -->
    </section><!-- end transparent-bg -->

	<section id="video-doc" class="white-wrapper">
		<div class="container">
			<div class="module clearfix">
				<div class="title text-center clearfix">
					<h2>HIGH QUALITY VIDEO DOCUMENTATION</h2>
					<hr>
                    <p class="lead">Don't Forget to check the video documentation, we added very useful and awesome clarity videos.<br>
 					It will very helpful for new bie and also very easy to understand</p>
				</div><!-- end title -->
                
                <div class="module clearfix aligncenter text-center">
                <img src="http://jollythemes.com/html/jollyall/images/demos/landing_page_06.png" class="wow fadeInUp" data-wow-duration="2s" alt="">
                </div>
  
			</div><!-- end content -->
		</div><!-- end container -->
    </section><!-- end white-wrapper -->

    <section class="grey-2-wrapper">
		<div class="container">
			<div class="module clearfix">
				<div class="title text-center clearfix">
					<h2>FOR REGULAR UPDATES & MORE ITEMS SUBSCRIE US</h2>
				</div><!-- end title -->
               <div class="row text-center">
                    <form id="loginform" class="clearfix" method="post" name="loginform">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" placeholder="Your Email Address">
                                </div>
                            </div>
                        </div>      	
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input type="text" class="form-control" placeholder="Your Phone Number">
                                </div>
                            </div>
                        </div>    
                        <div class="clearfix"></div><br>
                        <button class="btn btn-default btn-lg btn-transparent">Subscribe Now</button>    
                    </form>
                </div><br>
                
            </div><!-- end module -->
        </div><!-- end container -->
    </section><!-- end white-wrapper -->
    
	<footer id="footer-1" class="footer">
    	<div class="container">
        	<div class="clearfix">
    			<div class="footer-menu-2 text-center">
                	<ul>
                    	<li><a href="#">WHY CHOSSING JOLLYALL?</a></li>
                        <li><a href="#">FEATURES</a></li>
                        <li><a href="#">DEMOS</a></li>
                        <li><a href="#">VIDEO DOCUMENtation</a></li>
                    </ul>
                </div><!-- end footer-menu -->	
                
                <div class="footer_logo text-center aligncenter">
                	<img src="http://jollythemes.com/html/jollyall/images/footer_logo.png" alt="">
                </div>
                
                <div class="social-icons">
                    <span><a data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a></span>
                    <span><a data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a></span>
                    <span><a data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></span>
                </div><!-- end social icons -->
            </div><!-- end row -->
    	</div><!-- end container -->
    </footer><!-- end #footer-style-1 -->
  
  	<div class="dmtop">Scroll to Top</div>
      
<script src="<?php echo get_template_directory_uri();?>/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/retina.js"></script>
<script src="http://jollythemes.com/html/jollyall/js/smooth-scroll.js"></script>
<script src="http://jollythemes.com/html/jollyall/js/custom.js"></script>

<script>
(function($) {
"use strict";
	smoothScroll.init({
		speed: 1000,
		easing: 'easeInOutCubic',
		offset: 0,
		updateURL: true,
		callbackBefore: function ( toggle, anchor ) {},
		callbackAfter: function ( toggle, anchor ) {}
	});
})(jQuery);
</script>
    
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->   
<script type="text/javascript" src="http://jollythemes.com/html/jollyall/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script type="text/javascript" src="http://jollythemes.com/html/jollyall/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.tp-banner').show().revolution( {
		dottedOverlay:"none",
		delay:16000,
		startwidth:1170,
		startheight:700,
		hideThumbs:200,
		thumbWidth:100,
		thumbHeight:50,
		thumbAmount:5,
		navigationType:"bullet",
		navigationArrows:"solo",
		navigationStyle:"preview4",
		touchenabled:"on",
		onHoverStop:"on",
		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,
		parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
		keyboardNavigation:"off",
		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:20,
		soloArrowLeftHalign:"left",
		soloArrowLeftValign:"center",
		soloArrowLeftHOffset:20,
		soloArrowLeftVOffset:0,
		soloArrowRightHalign:"right",
		soloArrowRightValign:"center",
		soloArrowRightHOffset:20,
		soloArrowRightVOffset:0,
		shadow:0,
		fullWidth:"on",
		fullScreen:"on",
		spinner:"spinner2",
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,
		shuffle:"off",
		autoHeight:"off",
		forceFullWidth:"on",
		hideThumbsOnMobile:"off",
		hideNavDelayOnMobile:1500,
		hideBulletsOnMobile:"off",
		hideArrowsOnMobile:"off",
		hideThumbsUnderResolution:0,
		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0
	});
});	/*ready*/
</script>

<script src="http://jollythemes.com/html/jollyall/js/textrotate.js"></script>
<script>
(function($) {
"use strict";
	$("#rotateme").rotateme({ 
		text : ['Image', 'Video'], 
		interval : 3000  // in miliseconds
	});
})(jQuery);
</script>

	<!-- Portfolio Scripts-->
	<script src="http://jollythemes.com/html/jollyall/js/jquery.isotope.min.js"></script>
	<script>
	
		jQuery(window).on('load', function(){ var $ = jQuery;
        var $container = $('.masonry_wrapper'),
            colWidth = function () {
                var w = $container.width(), 
                    columnNum = 1,
                    columnWidth = 0;
                if (w > 1200) {
                    columnNum  = 4;
                } else if (w > 900) {
                    columnNum  = 4;
                } else if (w > 600) {
                    columnNum  = 3;
                } else if (w > 300) {
                    columnNum  = 2;
                }
                columnWidth = Math.floor(w/columnNum);
                $container.find('.item').each(function() {
                    var $item = $(this),
                        multiplier_w = $item.attr('class').match(/item-w(\d)/),
                        multiplier_h = $item.attr('class').match(/item-h(\d)/),
                        width = multiplier_w ? columnWidth*multiplier_w[1]-4 : columnWidth-4,
                        height = multiplier_h ? columnWidth*multiplier_h[1]*0.5-4 : columnWidth*0.5-4;
                    $item.css({
                        width: width,
                        height: height
                    });
                });
                return columnWidth;
            }
                        
            function refreshWaypoints() {
                setTimeout(function() {
                }, 1000);   
            }
                        
            $('nav.portfolio-filter ul a').on('click', function() {
                var selector = $(this).attr('data-filter');
                $container.isotope({ filter: selector }, refreshWaypoints());
                $('nav.portfolio-filter ul a').removeClass('active');
                $(this).addClass('active');
                return false;
            });
                
            function setPortfolio() { 
                setColumns();
                $container.isotope('reLayout');
            }
    
            isotope = function () {
                $container.isotope({
                    resizable: true,
                    itemSelector: '.item',
                    masonry: {
                        columnWidth: colWidth(),
                        gutterWidth: 0
                    }
                });
            };
        isotope();
        $(window).smartresize(isotope);
    }(jQuery));
	</script>
    

</body>
</html>