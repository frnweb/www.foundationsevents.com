<?php /* Template Name: One Page */
get_header('onepage') ;

$meta = _WSH()->get_meta();?>

<?php if( !sh_set( $meta, 'is_home' ) ) get_template_part( 'content', 'header' ); ?>

<?php while( have_posts() ): the_post(); ?>
     <?php the_content(); ?>
<?php endwhile;  ?>

<?php get_footer() ; ?>