<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>

	<div class="col-md-5 col-sm-6 col-xs-12">
		
		<div class="shop-item">

			<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
				//woocommerce_show_product_images();
			?>
	
		</div>
	
	</div>
	
	<div class="col-md-7 col-sm-6 col-xs-12">
		
		<div class="title">
			<h2><?php the_title(); ?></h2>
		</div>
		
		<div class="shop-meta clearfix">
			<div class="reviews">
				<?php woocommerce_template_single_rating(); ?>
			</div><!-- end reviews -->
		</div>
		
		<div class="mini-title">
			<h3><?php _e('Product Details:', SH_NAME); ?></h3>
		</div>
		
		<div class="desc">
			<?php woocommerce_template_single_excerpt(); ?>
		</div>
		
		<div class="mini-title">
			<h3><?php _e('Categories:', SH_NAME); ?></h3>
		</div>
		
		<div class="product-cats clearfix">
			<?php woocommerce_template_single_meta(); ?>
		</div>
		
		<div class="buynow clearfix">
			<?php woocommerce_template_single_add_to_cart(); ?>
		</div>
		
		<div class="clearfix">
			<?php woocommerce_template_single_sharing(); ?>
		</div>

			<?php
				/**
				 * woocommerce_single_product_summary hook
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>
		
		</div>

	<hr />
	<div class="clearfix"></div>
	<div class="module-widget">
		<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
