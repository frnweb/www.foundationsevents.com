<?php
define('DOMAIN' , 'wp_jollyall');
define('SH_NAME', 'wp_jollyall');
define('SH_VERSION', '1.0');
define('SH_ROOT', get_template_directory().'/');
define('SH_URL', get_template_directory_uri().'/');

include_once('includes/loader.php');
add_action('after_setup_theme', 'sh_theme_setup');

function sh_theme_setup()
{
	global $wp_version;
	
	load_theme_textdomain(SH_NAME, get_template_directory() . '/languages');
	add_editor_style();
	
	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support( 'post-formats', array( 'gallery', 'image', 'quote', 'video', 'audio' ) );
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( 'woocommerce' );
	
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => __('Main Menu', SH_NAME),
				'sidebar_menu' => __('Sidebar Menu', SH_NAME),
				'footer_menu' => __('Footer Menu', SH_NAME),
			)
		);
	}
	
	if ( ! isset( $content_width ) ) $content_width = 960;
	
	add_image_size( '265x265', 265, 265, true );
	add_image_size( '600x600', 600, 600, true );
	add_image_size( '534x265', 534, 265, true ); 
	add_image_size( '501x331', 501, 331, true );
	add_image_size( '471x414', 471, 414, true );
	add_image_size( '268x280', 268, 280, true );
	add_image_size( '600x489', 600, 489, true );
	add_image_size( '880x488', 880, 488, true );
	add_image_size( '265x372', 265, 372, true );
	add_image_size( '464x306', 464, 306, true );
	add_image_size( '434x570', 434, 570, true );
	add_image_size( '89x87', 89, 87, true );
	
	
	
}


function sh_widget_init()
{
	global $wp_registered_sidebars;
	
	$theme_options = _WSH()->option();
	
	//register_widget("SH_Recent_Post");
	//register_widget( 'SH_About_us' );
	//register_widget( 'SH_Twitter' );
	register_widget( 'SH_About_us' );
	register_widget( 'SH_ContactUs' );
	register_widget( 'SH_Twitter' );
	register_widget( 'SH_feedburner' );
	register_widget( 'SH_Keep_intouch' );
	register_widget( 'SH_Recent_Post_img' );
	//register_widget( 'SH_Tabber' );
	//register_widget( 'SH_Recent_Post_rating' );
	
	register_sidebar(array(
	  'name' => __( 'Default Sidebar', SH_NAME ),
	  'id' => 'default-sidebar',
	  'description' => __( 'Widgets in this area will be shown on the right-hand side.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="widget clearfix %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	
	register_sidebar(array(
	  'name' => __( 'Footer Sidebar', SH_NAME ),
	  'id' => 'footer-sidebar',
	  'description' => __( 'Widgets in this area will be shown in Footer Area.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 %2$s"><div class="widget">',
	  'after_widget'=>'</div></div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	register_sidebar(array(
	  'name' => __( 'Footer 1', SH_NAME ),
	  'id' => 'footer-sidebar-1',
	  'description' => __( 'Widgets in this area will be shown in Footer Area.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="col-md-6 col-sm-6 col-xs-12 %2$s"><div class="widget">',
	  'after_widget'=>'</div></div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	register_sidebar(array(
	  'name' => __( 'Footer 2', SH_NAME ),
	  'id' => 'footer-sidebar-2',
	  'description' => __( 'Widgets in this area will be shown in Footer Area.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 %2$s"><div class="widget">',
	  'after_widget'=>'</div></div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	register_sidebar(array(
	  'name' => __( 'Blog Listing', SH_NAME ),
	  'id' => 'blog-sidebar',
	  'description' => __( 'Widgets in this area will be shown on the right-hand side.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div class="widget">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="title"><h3>',
	  'after_title' => '</h3></div>'
	));
	
	$sidebars = sh_set(sh_set( $theme_options, 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 
	foreach( array_filter((array)$sidebars) as $sidebar)
	{
		if(sh_set($sidebar , 'topcopy')) continue ;
		
		$name = sh_set( $sidebar, 'sidebar_name' );
		
		if( ! $name ) continue;
		$slug = sh_slug( $name ) ;
		
		register_sidebar( array(
			'name' => $name,
			'id' =>  $slug ,
			'before_widget' => '<div class="widget">',
			'after_widget' => "</div>",
			'before_title' => '<div class="title"><h3>',
			'after_title' => '</h3></div>',
		) );		
	}
	
	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;
}
add_action( 'widgets_init', 'sh_widget_init' );


// Update items in cart via AJAX
add_filter('add_to_cart_fragments', 'sh_woo_add_to_cart_ajax');
function sh_woo_add_to_cart_ajax( $fragments ) {
    
	global $woocommerce;
    
	/*ob_start();?>
	
	<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>">
		<?php echo sprintf(_n('%d item ', '%d items ', $woocommerce->cart->cart_contents_count, SH_NAME), $woocommerce->cart->cart_contents_count); ?> 
		- <?php echo WC()->cart->get_cart_subtotal(); ?>
	</a>
    <?php //$fragments['a.cart-contents'] = ob_get_clean();
	ob_end_clean();*/
	
	
	ob_start();
	
	include('includes/modules/wc_cart.php' );
	
	$fragments['div.wc-header-cart'] = ob_get_clean();
	
    return $fragments;
}

add_filter( 'woocommerce_enqueue_styles', '__return_false' );


add_shortcode( 'sh_animate_it', '_sh_animate_it' );

function _sh_animate_it( $atts, $contents = null )
{
	return include( 'includes/modules/shortcodes/animate_it.php' );
}


