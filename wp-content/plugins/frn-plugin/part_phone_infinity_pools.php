<?php



add_action( 'init', 'frn_load_pools' );
function frn_load_pools() {
	//This feature runs whether or not infinity is activated
	//Due to shortcodes, we need the array active to make sure the SCs don't show up when deactivated
	global $infinity_pools;
	$infinity_pools=frn_infinity_pools_list();
}




function frn_infinity_pools_list($type="all") {
	//This feature runs whether or not infinity is activated
	//Due to shortcodes, we need the array active to make sure the SCs don't show up when deactivated
	$pools = array();
	if($type!=="tags") {
		//"local" versions of the inpatient pools simply append a "_local" portion to the core shortcode. That makes it easier to associate.
		$pools = array(
						// (0) TD number, (1) pool name, (2) facility type, (3) shortcode, (4) keyword for auto triggers (5) type of number that Infinity will show
			'0'     => array('number' => '6154909376','mitel' => '8552707615','sms' => '8556551737','name' => 'Foundations Recovery Network','type' => 'Corporate','sc' => 'frn','kw' => 'network','replaced' => 'toll-free'),
			'1'     => array('number' => '7069142327','mitel' => '7705573565','sms' => '8557345244','name' => 'Black Bear Lodge','type' => 'Inpatient','sc' => 'blackbear','kw' => 'bear','replaced' => 'toll-free'),
			'2'     => array('number' => '7605484032','mitel' => '7604596138','sms' => '8556508346','name' => 'Michael\'s House','type' => 'Inpatient','sc' => 'mhouse','kw' => 'house','replaced' => 'toll-free'),
			'3'     => array('number' => '2692804673','mitel' => '8553179223','sms' => '8555597427','name' => 'Skywood Recovery','type' => 'Inpatient','sc' => 'skywood','kw' => 'sky','replaced' => 'toll-free'),
			'4'     => array('number' => '6782513189','mitel' => '8773453301','sms' => '8554110562','name' => 'Talbott Recovery','type' => 'Inpatient','sc' => 'talbott','kw' => 'tal','replaced' => 'toll-free'),
			'5'     => array('number' => '4243873118','mitel' => '4244655088','sms' => '4242671670','name' => 'The Canyon at Peace Park','type' => 'Inpatient','sc' => 'canyon','kw' => 'canyon','replaced' => 'local'),
			'6'     => array('number' => '9013504575','mitel' => '9015056518','sms' => '8557595997','name' => 'The Oaks Treatment','type' => 'Inpatient','sc' => 'oaks','kw' => 'oaks','replaced' => 'toll-free'),
			'7'     => array('number' => '6292053763','mitel' => '8558086210','sms' => '8558107223','name' => 'Foundations Events','type' => 'Brand','sc' => 'events','kw' => 'events','replaced' => 'toll-free'),
			'8'     => array('number' => '6292053447','mitel' => '8883124220','sms' => '8553420869','name' => 'Heroes In Recovery','type' => 'Brand','sc' => 'hir','kw' => 'heros','replaced' => 'toll-free'),
			'9'     => array('number' => '6292053760','mitel' => '8777141322','sms' => '8558113822','name' => 'The Life Challenge (L+C)','type' => 'Brand','sc' => 'life','kw' => 'alumni','replaced' => 'toll-free'),
			'10'     => array('number' => '6782643976','mitel' => '4044607101','sms' => '4044801808','name' => 'IOP: Foundations Atlanta at Midtown','type' => 'Outpatient','sc' => 'iop_midtown','kw' => 'midtown','replaced' => 'local'),
			'11'     => array('number' => '4702644673','mitel' => '7705573547','sms' => '4702207101','name' => 'IOP: Foundations Atlanta at Roswell','type' => 'Outpatient','sc' => 'iop_roswell','kw' => 'roswell','replaced' => 'local'),
			'12'     => array('number' => '3125164325','mitel' => '3125964088','sms' => '3125867072','name' => 'IOP: Foundations Chicago','type' => 'Outpatient','sc' => 'iop_chicago','kw' => 'chicago','replaced' => 'local'),
			'13'     => array('number' => '2486176237','mitel' => '2489656614','sms' => '2489757080','name' => 'IOP: Foundations Detroit','type' => 'Outpatient','sc' => 'iop_detroit','kw' => 'det','replaced' => 'local'),
			'14'     => array('number' => '9013504520','mitel' => '9015056519','sms' => '9015633011','name' => 'IOP: Foundations Memphis','type' => 'Outpatient','sc' => 'iop_memphis','kw' => 'mem','replaced' => 'local'),
			'15'     => array('number' => '6154549209','mitel' => '6153706880','sms' => '6158140093','name' => 'IOP: Foundations Nashville','type' => 'Outpatient','sc' => 'iop_nashville','kw' => 'nash','replaced' => 'local'),
			'16'     => array('number' => '6194314673','mitel' => '6193211575','sms' => '6196950114','name' => 'IOP: Foundations San Diego','type' => 'Outpatient','sc' => 'iop_diego','kw' => 'diego','replaced' => 'local'),
			'17'     => array('number' => '4158546735','mitel' => '4152931680','sms' => '6285009019','name' => 'IOP: Foundations San Francisco','type' => 'Outpatient','sc' => 'iop_sanfran','kw' => 'fran','replaced' => 'local'),
			'18'     => array('number' => '7605234357','mitel' => '7604596136','sms' => '7602791366','name' => 'IOP: Michael\'s House Outpatient','type' => 'Outpatient','sc' => 'iop_mhouse','kw' => 'house outpatient','replaced' => 'local'),
			'19'     => array('number' => '7065234673','mitel' => '7062004004','sms' => '7069956065','name' => 'IOP: Talbott Recovery Columbus','type' => 'Outpatient','sc' => 'iop_columbus','kw' => 'col','replaced' => 'local'),
			'20'     => array('number' => '4703800299','mitel' => '4042701023','sms' => '6789495054','name' => 'IOP: Talbott Recovery Dunwoody','type' => 'Outpatient','sc' => 'iop_dunwoody','kw' => 'dun','replaced' => 'local'),
			'21'     => array('number' => '8187405980','mitel' => '8184641325','sms' => '8186979146','name' => 'IOP: The Canyon at Encino','type' => 'Outpatient','sc' => 'iop_encino','kw' => 'encino','replaced' => 'local'),
			'22'     => array('number' => '4246108816','mitel' => '4244655066','sms' => '4245677131','name' => 'IOP: The Canyon at Santa Monica','type' => 'Outpatient','sc' => 'iop_monica','kw' => 'monic','replaced' => 'local')
		);
	}
	if(($type=="tags" || $type=="all") && $type!=="no_tags") {
		$tags=array(		//discovery number, pool name, type, shortcode, kw for searching (or local for local numbers), what we should see from Infinity
			'23'     => array('number' => '6153714163','mitel' => '6153714163','sms' => '','name' => 'Foundations Recovery Network (Local)','type' => 'Corporate','sc' => 'frn_local','kw' => '[none]','replaced' => 'local'),
			'24'     => array('number' => '7705573566','mitel' => '7705573566','sms' => '','name' => 'Black Bear Lodge (Local)','type' => 'Inpatient','sc' => 'blackbear_local','kw' => '[none]','replaced' => 'local'),
			'25'     => array('number' => '6153715723','mitel' => '6153715723','sms' => '','name' => 'Michael\'s House (Local)','type' => 'Inpatient','sc' => 'mhouse_local','kw' => '[none]','replaced' => 'local'),
			'26'     => array('number' => '6153728938','mitel' => '6153728938','sms' => '','name' => 'The Oaks Treatment (Local)','type' => 'Inpatient','sc' => 'oaks_local','kw' => '[none]','replaced' => 'local'),
			'27'     => array('number' => '8884189657','mitel' => '8884189657','sms' => '','name' => 'Talbott Recovery (Local)','type' => 'Inpatient','sc' => 'talbott_local','kw' => '[none]','replaced' => 'local'),
			'28'     => array('number' => '6153714147','mitel' => '6153714147','sms' => '','name' => 'Skywood Recovery (Local)','type' => 'Inpatient','sc' => 'skywood_local','kw' => '[none]','replaced' => 'local'),
			'29'     => array('number' => '6292053767','mitel' => '8888981989','sms' => '','name' => '[BD] Business Development','type' => 'Corporate','sc' => 'bd','kw' => 'business','replaced' => 'toll-free')
		);
		foreach($tags as $key=>$tag)
		{
		    $pools[$key] = $tag;
		}
	}
	//uasort($pools,"frn_infinity_array_sort");
	return $pools;
}

function frn_infinity_array_sort($a,$b) {
    $type_sort = $a['type'] < $b['type'];
    if ($a['type'] == $b['type']) {
        if ($a['name'] == $b['name']) {
	        return 0;
	    }
	    return ($a['name'] < $b['name']) ? -1 : 1;
    }
    return ($a['type'] < $b['type']) ? -1 : 1;
}

function frn_phone_patterns($number="",$type="()") {
	/*
		NOTES: we also have the following functions depending on how undisciplined the input is. These are in part_phone_number_features.php.
			frn_phone_for_links($number) - produces only a dashed output of the number, strips everything, HTML code, etc down to just the digits. uses a preg_replace option to strip everything out, but doubles it depending on how the number was discovered.
			frn_phone_digits_only($number) - keeps the phone number pattern provided, but removes all HTML and scripts

		This is a more lightweight feature that never strips HTML and expects a controlled input from us, but allows re-formatting flexibility.
	*/
	if(trim($type)!=="") { //if blank, it'll pass through the original format -- no processing (fallback just in case ever needed)
		$chars  = array('(', ')', ' ', '-');
		$number = str_replace($chars,"",$number); //removes "(", ")", spaces, and dashes (required for proper formatting below)
		if(trim($type)!=="none") { //if none, then no phone number formatting will be applied, it'll just be a string of numbers 
			if($type=="1-"      || $type=="1-###-###-####")   $format = '1-$1-$2-$3'; 		//a "1" always starts the dashed number
			elseif($type=="-"   || $type=="###-###-####")	  $format = '$1-$2-$3'; 		//no "1" at the beginning for dashed (maybe good for local numbers)
			elseif($type=="1."  || $type=="1.###.###.####")   $format = '1\.$1\.$2\.$3'; 	//a "1" always starts the period delimited number
			elseif($type=="."   || $type=="###.###.####") 	  $format = '$1\.$2\.$3'; 		//no "1" at beginning
			elseif($type=="1()" || $type=="1 (###) ###-####") $format = '1 ($1) $2-$3'; 	//a "1" always starts the number
			elseif($type=="()" || $type=="(###) ###-####")    $format = '($1) $2-$3'; 
			else $format = $type; 													//(DEFAULT) no "1" starts it 
			if(strlen($number)==10) $pattern = '/([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
				else 				$pattern = '/[0-9]([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
			$number = preg_replace($pattern,$format,$number);
		}
	}
	return $number;
}